'use strict'
app
        .controller('menuCtrl', ['$state', '$scope', 'config', 'UsuarioService', 'PersonaService', '$cookies', '$base64', 'RolService', 'UsuarioEdificioService', function ($state, $scope, config, UsuarioService, PersonaService, $cookies, $base64, RolService, UsuarioEdificioService) {

                var myMapMenu = new Map();
                var dataMenuLis = ['admin_usuario_edificio', 'admin_persona', 'admin_usuario', 'admin_rol', 'admin_finger', 'admin_acceso', 'admin_ciudad', 'admin_marca', 'admin_edificio', 'admin_tipo_edificio',
                    'admin_dependencia', 'admin_placa', 'admin_tipo_placa', 'admin_tipo_placa_pin', 'admin_componente', 'admin_tipo_componente',
                    'admin_categoria', 'admin_tarifa', 'admin_prueba_consumo', 'admin_prueba_consumo_gral', 'admin_consumo_especifico', 'admin_consumo_gral', 'admin_nodo', 'admin_nodo_pin',
                    'admin_parametro', 'admin_plan', 'admin_sensor',
                    'admin_administracion', 'admin_temperatura', 'admin_humo', 'admin_movimiento', 'admin_humedad', 'admin_manejo_control'];

                $scope.cargarAdminEdificio = function (id) {
                    UsuarioEdificioService.recuperarCantEdificio(id).success(function (data) {
                        if (data !== 0) {
                            $scope.edificioCount = " " + data;
                        } else {
                            $scope.edificioCount = " 0";
                        }
//                        $("#admin_persona").css({"pointer-events": "none", "opacity": "0.3"});
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $scope.salirData = function () {
                    $cookies.remove("estado");
                    $cookies.remove("token", null);
                    $cookies.remove("facultad", null);
                    $cookies.remove("idusuario", null);
                    $cookies.remove("usuario", null);
                    $cookies.remove("detalles", null);
                    location.href = "../#";
                };

                if ($cookies.getObject("estado") === null || $cookies.getObject("estado") === undefined) {
                    $scope.salirData();
                }

                $scope.addClase = function (val) {
                    var menues = ['usu', 'mat'];
                    for (var i = 0; i < menues.length; i++) {
                        if (val.toLowerCase() === menues[i].toLowerCase()) {
                            $("#" + val).addClass("active");
                        } else {
                            $("#" + menues[i]).removeClass("active");
                        }
                    }
                };

                $scope.cargarListaMenu = function (id) {
                    UsuarioService.listarPorId(id).success(function (data) {
                        if (data.usuarioRols.length > 0) {
                            for (var i = 0, max = data.usuarioRols.length; i < max; i++) {
                                $scope.rolFunc = data.usuarioRols[i].rol.rolFuncions;
                                if ($scope.rolFunc.length <= 0) {
                                    for (var i = 0; i < dataMenuLis.length; i++) {
                                        $("#" + dataMenuLis[i]).css({"pointer-events": "none", "color": "gray"});
                                    }
                                } else {
                                    var datos = 0;
                                    for (var i = 0, max = $scope.rolFunc.length; i < max; i++) {
                                        datos++;
                                        var descriFun = $scope.rolFunc[i].funcion.url;
                                        myMapMenu.set(descriFun, descriFun);
                                    }
                                    if (datos === $scope.rolFunc.length) {
                                        for (var i = 0; i < dataMenuLis.length; i++) {
                                            if (!myMapMenu.has(dataMenuLis[i])) {
                                                if (dataMenuLis[i] === "admin_usuario") {
                                                    $("#usuario01").css({"pointer-events": "none", "color": "gray"});
                                                    $("#usuario02").css({"pointer-events": "none", "color": "gray"});
                                                }
                                                $("#" + dataMenuLis[i]).css({"pointer-events": "none", "color": "gray"});
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            for (var i = 0; i < dataMenuLis.length; i++) {
                                if (!myMapMenu.has(dataMenuLis[i])) {
                                    if (dataMenuLis[i] === "admin_usuario") {
                                        $("#usuario01").css({"pointer-events": "none", "color": "gray"});
                                        $("#usuario02").css({"pointer-events": "none", "color": "gray"});
                                    }
                                    $("#" + dataMenuLis[i]).css({"pointer-events": "none", "color": "gray"});
                                }
                            }
                        }
                    });
                };


                $scope.cargarMensajes = function () {
                    UsuarioService.getCountAllConfirm().success(function (data) {
                        if (data !== 0) {
                            $scope.registerCount = " " + data;
                            $("#mens").html('<span class="label label-danger"><span class="glyphicon glyphicon-envelope"></span></span>');
                            $("#mensajes_view").show();
                        } else {
                            $("#mens").html('<span class="label label-default"><span class="glyphicon glyphicon-envelope"></span></span>');
                            $("#mensajes_view").hide();
                        }

                        $("#fingerPrint_view").show();
//                    console.log("DATO A RECUPERAR "+data);
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                var getUrlParameter = function getUrlParameter(sParam) {
                    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                            sURLVariables = sPageURL.split('&'),
                            sParameterName,
                            i;
                    for (i = 0; i < sURLVariables.length; i++) {
                        sParameterName = sURLVariables[i].split(sParam + "=");
                        return sParameterName[1];
                    }
                };
                //ORIGINAL

                $scope.reload = function () {
                    location.reload();
//                    $state.reload(); recarga el state
                };
            }])
        .controller('listUsuariosCtrl', ['$state', '$scope', 'config', 'UsuarioService', '$cookies', '$base64', "$filter", '$http', '$rootScope', function ($state, $scope, config, UsuarioService, $cookies, $base64, $filter, $http, $rootScope) {
//                console.log("HOLAAA");

                $scope.materias = {};
                var mapUsuarios = new Map();
                var mapUsuariosJSON = new Map();
                var mapUsuariosParent = new Map();
                var mapCate1 = new Map();
                var mapCate2 = new Map();
                var mapCate3 = new Map();
                var mapCate4 = new Map();
                var mapCate5 = new Map();

                var mapCateMod1 = new Map();
                var mapCateMod2 = new Map();
                var mapCateMod3 = new Map();
                var mapCateMod4 = new Map();
                var mapCateMod5 = new Map();
                $scope.personaAgregar = {};
                $scope.personaModificar = {};

                $scope.people = [];

                $("#paginaActual").val("1");
                $scope.totalItems = 0;
                $scope.currentPage = 0;
                $scope.pageChangedFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };
                $scope.pageChangedSinFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };

                $("#primeraCategoria").change(function () {
                    $("#cateTwo").css("display", "none");
                    $("#cateTwoS").css("display", "none");
                    $scope.cate2 = [];
                    mapCate2 = new Map();
                    $scope.idCate2 = "";

                    $("#cateThree").css("display", "none");
                    $("#cateThreeS").css("display", "none");
                    $scope.cate3 = [];
                    mapCate3 = new Map();
                    $scope.idCate3 = "";

                    $("#cateFour").css("display", "none");
                    $("#cateFourS").css("display", "none");
                    $scope.cate4 = [];
                    mapCate4 = new Map();
                    $scope.idCate4 = "";

                    $("#cateFive").css("display", "none");
                    $("#cateFiveS").css("display", "none");
                    $scope.cate5 = [];
                    mapCate5 = new Map();
                    $scope.idCate5 = "";


                    UsuarioService.listarCategoriasFilter($("#primeraCategoria").val()).success(function (data) {
                        $scope.cate2 = [];
                        angular.forEach(data, function (value, key) {

                            if (parseInt(value.parent) === parseInt($("#primeraCategoria").val())) {
                                $scope.cate2.push(value.name);
                                mapCate2.set(value.name, value.id);
                            }
                        });
                        $("#cateTwo").css("display", "inline");
                        $("#cateTwoS").css("display", "inline");

                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                });

                $("#primeraCategoriaMod").change(function () {
                    $("#cateTwoMod").css("display", "none");
                    $("#cateTwoSMod").css("display", "none");
                    $scope.cate2Mod = [];
                    mapCateMod2 = new Map();
                    $scope.idCate2Mod = "";

                    $("#cateThreeMod").css("display", "none");
                    $("#cateThreeSMod").css("display", "none");
                    $scope.cate3Mod = [];
                    mapCateMod3 = new Map();
                    $scope.idCate3Mod = "";

                    $("#cateFourMod").css("display", "none");
                    $("#cateFourSMod").css("display", "none");
                    $scope.cate4Mod = [];
                    mapCateMod4 = new Map();
                    $scope.idCate4Mod = "";

                    $("#cateFiveMod").css("display", "none");
                    $("#cateFiveSMod").css("display", "none");
                    $scope.cate5Mod = [];
                    mapCateMod5 = new Map();
                    $scope.idCate5Mod = "";


                    UsuarioService.listarCategoriasFilter($("#primeraCategoriaMod").val()).success(function (data) {
                        $scope.cate2Mod = [];
                        angular.forEach(data, function (value, key) {

                            if (parseInt(value.parent) === parseInt($("#primeraCategoriaMod").val())) {
                                $scope.cate2Mod.push(value.name);
                                mapCateMod2.set(value.name, value.id);
                            }
                        });
                        $("#cateTwoMod").css("display", "inline");
                        $("#cateTwoSMod").css("display", "inline");

                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                });

                $scope.segundaCategoria = function () {
                    $("#cateThree").css("display", "none");
                    $("#cateThreeS").css("display", "none");
                    $scope.cate3 = [];
                    mapCate3 = new Map();
                    $scope.idCate3 = "";

                    $("#cateFour").css("display", "none");
                    $("#cateFourS").css("display", "none");
                    $scope.cate4 = [];
                    mapCate4 = new Map();
                    $scope.idCate4 = "";

                    $("#cateFive").css("display", "none");
                    $("#cateFiveS").css("display", "none");
                    $scope.cate5 = [];
                    mapCate5 = new Map();
                    $scope.idCate5 = "";

                    UsuarioService.listarCategoriasFilter(mapCate2.get($scope.idCate2)).success(function (data) {
                        $scope.cate3 = [];
                        angular.forEach(data, function (value, key) {

                            if (parseInt(value.parent) === parseInt(mapCate2.get($scope.idCate2))) {
                                $scope.cate3.push(value.name);
                                mapCate3.set(value.name, value.id);
                            }
                        });
                        $("#cateThree").css("display", "inline");
                        $("#cateThreeS").css("display", "inline");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }

                $scope.segundaCategoriaMod = function () {
                    $("#cateThreeMod").css("display", "none");
                    $("#cateThreeSMod").css("display", "none");
                    $scope.cate3Mod = [];
                    mapCateMod3 = new Map();
                    $scope.idCate3Mod = "";

                    $("#cateFourMod").css("display", "none");
                    $("#cateFourSMod").css("display", "none");
                    $scope.cate4Mod = [];
                    mapCateMod4 = new Map();
                    $scope.idCate4Mod = "";

                    $("#cateFiveMod").css("display", "none");
                    $("#cateFiveSMod").css("display", "none");
                    $scope.cate5Mod = [];
                    mapCateMod5 = new Map();
                    $scope.idCate5Mod = "";

                    UsuarioService.listarCategoriasFilter(mapCateMod2.get($scope.idCate2Mod)).success(function (data) {
                        $scope.cate3Mod = [];
                        angular.forEach(data, function (value, key) {

                            if (parseInt(value.parent) === parseInt(mapCateMod2.get($scope.idCate2Mod))) {
                                $scope.cate3Mod.push(value.name);
                                mapCateMod3.set(value.name, value.id);
                            }
                        });
                        $("#cateThreeMod").css("display", "inline");
                        $("#cateThreeSMod").css("display", "inline");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }

                $scope.terceraCategoria = function () {
                    $("#cateFour").css("display", "none");
                    $("#cateFourS").css("display", "none");
                    $scope.cate4 = [];
                    mapCate4 = new Map();
                    $scope.idCate4 = "";

                    $("#cateFive").css("display", "none");
                    $("#cateFiveS").css("display", "none");
                    $scope.cate5 = [];
                    mapCate5 = new Map();
                    $scope.idCate5 = "";

                    console.log(mapCate3.get($scope.idCate3));
                    UsuarioService.listarCategoriasFilter(mapCate3.get($scope.idCate3)).success(function (data) {
                        $scope.cate4 = [];
                        angular.forEach(data, function (value, key) {

                            if (parseInt(value.parent) === parseInt(mapCate3.get($scope.idCate3)) && value.visible === 1) {
                                $scope.cate4.push(value.name);
                                mapCate4.set(value.name, value.id);
                            }
                            $("#cateFour").css("display", "inline");
                            $("#cateFourS").css("display", "inline");
                        });

                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }

                $scope.terceraCategoriaMod = function () {
                    $("#cateFourMod").css("display", "none");
                    $("#cateFourSMod").css("display", "none");
                    $scope.cate4Mod = [];
                    mapCateMod4 = new Map();
                    $scope.idCate4Mod = "";

                    $("#cateFiveMod").css("display", "none");
                    $("#cateFiveSMod").css("display", "none");
                    $scope.cate5Mod = [];
                    mapCateMod5 = new Map();
                    $scope.idCate5Mod = "";

                    UsuarioService.listarCategoriasFilter(mapCateMod3.get($scope.idCate3Mod)).success(function (data) {
                        $scope.cate4Mod = [];
                        angular.forEach(data, function (value, key) {

                            if (parseInt(value.parent) === parseInt(mapCateMod3.get($scope.idCate3Mod)) && value.visible === 1) {
                                $scope.cate4Mod.push(value.name);
                                mapCateMod4.set(value.name, value.id);
                            }
                            $("#cateFourMod").css("display", "inline");
                            $("#cateFourSMod").css("display", "inline");
                        });

                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }

                $scope.cuartaCategoria = function () {

                    $("#cateFive").css("display", "none");
                    $("#cateFiveS").css("display", "none");
                    $scope.cate5 = [];
                    mapCate5 = new Map();
                    $scope.idCate5 = "";

                    UsuarioService.listarCategoriasFilter(mapCate4.get($scope.idCate4)).success(function (data) {
                        $scope.cate5 = [];
                        angular.forEach(data, function (value, key) {

                            if (parseInt(value.parent) === parseInt(mapCate4.get($scope.idCate4)) && value.visible === 1) {
                                $scope.cate5.push(value.name);
                                mapCate5.set(value.name, value.id);
                            }
                            $("#cateFive").css("display", "inline");
                            $("#cateFiveS").css("display", "inline");
                        });

                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }

                $scope.cuartaCategoriaMod = function () {

                    $("#cateFiveMod").css("display", "none");
                    $("#cateFiveSMod").css("display", "none");
                    $scope.cate5Mod = [];
                    mapCateMod5 = new Map();
                    $scope.idCate5Mod = "";

                    UsuarioService.listarCategoriasFilter(mapCateMod4.get($scope.idCate4Mod)).success(function (data) {
                        $scope.cate5Mod = [];
                        angular.forEach(data, function (value, key) {

                            if (parseInt(value.parent) === parseInt(mapCateMod4.get($scope.idCate4Mod)) && value.visible === 1) {
                                $scope.cate5Mod.push(value.name);
                                mapCateMod5.set(value.name, value.id);
                            }
                            $("#cateFiveMod").css("display", "inline");
                            $("#cateFiveSMod").css("display", "inline");
                        });

                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }

                $scope.ocultarCate2 = function () {
                    $("#cateTwo").css("display", "none");
                    $("#cateTwoS").css("display", "none");
                    $scope.cate2 = [];
                    mapCate2 = new Map();
                    $scope.idCate2 = "";

                    $("#cateThree").css("display", "none");
                    $("#cateThreeS").css("display", "none");
                    $scope.cate3 = [];
                    mapCate3 = new Map();
                    $scope.idCate3 = "";

                    $("#cateFour").css("display", "none");
                    $("#cateFourS").css("display", "none");
                    $scope.cate4 = [];
                    mapCate4 = new Map();
                    $scope.idCate4 = "";

                    $("#cateFive").css("display", "none");
                    $("#cateFiveS").css("display", "none");
                    $scope.cate5 = [];
                    mapCate5 = new Map();
                    $scope.idCate5 = "";
                }

                $scope.ocultarCate2Mod = function () {
                    $("#cateTwoMod").css("display", "none");
                    $("#cateTwoSMod").css("display", "none");
                    $scope.cate2Mod = [];
                    mapCateMod2 = new Map();
                    $scope.idCate2Mod = "";

                    $("#cateThreeMod").css("display", "none");
                    $("#cateThreeSMod").css("display", "none");
                    $scope.cate3Mod = [];
                    mapCateMod3 = new Map();
                    $scope.idCate3Mod = "";

                    $("#cateFourMod").css("display", "none");
                    $("#cateFourSMod").css("display", "none");
                    $scope.cate4Mod = [];
                    mapCateMod4 = new Map();
                    $scope.idCate4Mod = "";

                    $("#cateFiveMod").css("display", "none");
                    $("#cateFiveSMod").css("display", "none");
                    $scope.cate5Mod = [];
                    mapCateMod5 = new Map();
                    $scope.idCate5Mod = "";
                }

                $scope.ocultarCate3 = function () {
                    $("#cateThree").css("display", "none");
                    $("#cateThreeS").css("display", "none");
                    $scope.cate3 = [];
                    mapCate3 = new Map();
                    $scope.idCate3 = "";

                    $("#cateFour").css("display", "none");
                    $("#cateFourS").css("display", "none");
                    $scope.cate4 = [];
                    mapCate4 = new Map();
                    $scope.idCate4 = "";

                    $("#cateFive").css("display", "none");
                    $("#cateFiveS").css("display", "none");
                    $scope.cate5 = [];
                    mapCate5 = new Map();
                    $scope.idCate5 = "";
                }

                $scope.ocultarCate3Mod = function () {
                    $("#cateThreeMod").css("display", "none");
                    $("#cateThreeSMod").css("display", "none");
                    $scope.cate3Mod = [];
                    mapCateMod3 = new Map();
                    $scope.idCate3Mod = "";

                    $("#cateFourMod").css("display", "none");
                    $("#cateFourSMod").css("display", "none");
                    $scope.cate4Mod = [];
                    mapCateMod4 = new Map();
                    $scope.idCate4Mod = "";

                    $("#cateFiveMod").css("display", "none");
                    $("#cateFiveSMod").css("display", "none");
                    $scope.cate5Mod = [];
                    mapCateMod5 = new Map();
                    $scope.idCate5Mod = "";
                }

                $scope.ocultarCate4 = function () {
                    $("#cateFour").css("display", "none");
                    $("#cateFourS").css("display", "none");
                    $scope.cate4 = [];
                    mapCate4 = new Map();
                    $scope.idCate4 = "";

                    $("#cateFive").css("display", "none");
                    $("#cateFiveS").css("display", "none");
                    $scope.cate5 = [];
                    mapCate5 = new Map();
                    $scope.idCate5 = "";
                }

                $scope.ocultarCate4Mod = function () {
                    $("#cateFourMod").css("display", "none");
                    $("#cateFourSMod").css("display", "none");
                    $scope.cate4Mod = [];
                    mapCateMod4 = new Map();
                    $scope.idCate4Mod = "";

                    $("#cateFiveMod").css("display", "none");
                    $("#cateFiveSMod").css("display", "none");
                    $scope.cate5Mod = [];
                    mapCateMod5 = new Map();
                    $scope.idCate5Mod = "";
                }

                $scope.ocultarCate5 = function () {
                    $("#cateFive").css("display", "none");
                    $("#cateFiveS").css("display", "none");
                    $scope.cate5 = [];
                    mapCate5 = new Map();
                    $scope.idCate5 = "";
                }

                $scope.ocultarCate5Mod = function () {
                    $("#cateFiveMod").css("display", "none");
                    $("#cateFiveSMod").css("display", "none");
                    $scope.cate5Mod = [];
                    mapCateMod5 = new Map();
                    $scope.idCate5Mod = "";
                }


                //               pagination
                $scope.listar = function () {
                    $scope.people = [];
//                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    UsuarioService.listarUsuarios().success(function (data) {
                        angular.forEach(data.users, function (value, key) {

//                            if (value.visible === 1) {
                            if (value.firstaccess !== 0) {
                                var date = new Date(value.firstaccess * 1000);

                                let dateMonth = ("0" + date.getDate()).slice(-2);
                                let month = ("0" + (date.getMonth() + 1)).slice(-2);
                                let year = date.getFullYear();
                                value.firstaccess = dateMonth + "-" + month + "-" + year;
                            } else {
                                value.firstaccess = "SIN FECHA";
                            }


                            var mostrar = true;
                            if (value.lastaccess !== 0) {
                                var dateHOY = new Date();
                                var dateEnd = new Date(value.lastaccess * 1000);

                                if (dateHOY > dateEnd) {
                                    mostrar = false;
                                }

                                let dateMonthEnd = ("0" + dateEnd.getDate()).slice(-2);
                                let monthEnd = ("0" + (dateEnd.getMonth() + 1)).slice(-2);
                                let yearEnd = dateEnd.getFullYear();
                                value.lastaccess = dateMonthEnd + "-" + monthEnd + "-" + yearEnd;
                            } else {
                                value.lastaccess = "SIN FECHA";
                            }



//                            if (mostrar === true) {

                            $scope.people.push(value);




                            $scope.nameFilter = '';
                            $scope.ageFilter = '';
                            $scope.stateFilter = '';

                            $scope.itemsPerPage = 20;



                            $scope.currentPage = 1;
                            console.debug($scope.totalItems);
                            console.debug(Math.ceil($scope.people.length / $scope.itemsPerPage));
                            $scope.noOfPages = Math.ceil($scope.people.length / $scope.itemsPerPage);
                            console.debug($scope.noOfPages)
//                            }
//                            }
                        });
//                        $scope.paginar();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };
                $scope.paginar = function () {
                    UsuarioService.count().success(function (data) {
                        $scope.catidad = data;
                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "none");
                        $("#paginacionSinFiltro").css("display", "inline");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $rootScope.$on("$locationChangeStart", function (event, data) {
                    var pag = parseInt(data.replace("http://" + config.ipFront + ":8017/menu.html#/:pagina=", ""));
                    if (!isNaN(pag)) {
                        $("#paginaActual").val(pag);
                        $scope.buscarSegunPagina();
                    }
                });

                $scope.paginarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.apellido === "" || $scope.apellido === undefined) {
                        $scope.ape = null;
                    } else {
                        $scope.ape = $scope.apellido;
                    }
                    if ($scope.ci === "" || $scope.ci === undefined) {
                        $scope.ced = null;
                    } else {
                        $scope.ced = $scope.ci;
                    }
                    UsuarioService.countFiltro($scope.filt, $scope.ape, $scope.ced).success(function (data) {
                        $scope.catidad = data;
                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "inline");
                        $("#paginacionSinFiltro").css("display", "none");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
//                    $("#paginacionesPersonas").html('<ul class="pagination"><li class="disabled"><a href="#">&laquo;</a></li><li class="active"><a href="#">1</a></li><li ><a href="#">2</a></li><li><a href="#">&raquo;</a></li></ul>');
                };

                $scope.listarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.apellido === "" || $scope.apellido === undefined) {
                        $scope.ape = null;
                    } else {
                        $scope.ape = $scope.apellido;
                    }
                    if ($scope.ci === "" || $scope.ci === undefined) {
                        $scope.ced = null;
                    } else {
                        $scope.ced = $scope.ci;
                    }

                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    UsuarioService.fetchFiltro(config.cantPaginacion, offSet, $scope.filt, $scope.ape, $scope.ced).success(function (data) {
                        $scope.materias = data;
                        $scope.paginarConFiltro();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }

                $scope.buscar = function () {
                    $scope.materias = [];

                    UsuarioService.listarUsuarios().success(function (data) {
                        var num = data.length;
                        var i = 0;
                        angular.forEach(data, function (value, key) {
                            mapUsuarios.set(value.id, value.name);
                            mapUsuariosParent.set(value.id, value.parent);
                            mapUsuariosJSON.set(value.id, value);
                            i++;
                            if (i === num) {
                                $scope.listar();
                            }
                        });
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });

                };

                $scope.buscarSegunPagina = function () {
                    $scope.materias = [];
                    $scope.listar();
                };
//                FIN DE PAGINACION

                $scope.generarReporte = function () {
                    $scope.jsonData = $cookies.getObject("usuarioLogueado");
                    $scope.userLog = $scope.jsonData.usuarioUsuario;

                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.apellido === "" || $scope.apellido === undefined) {
                        $scope.ape = null;
                    } else {
                        $scope.ape = $scope.apellido;
                    }
                    if ($scope.ci === "" || $scope.ci === undefined) {
                        $scope.ced = null;
                    } else {
                        $scope.ced = $scope.ci;
                    }

                    $http.get(config.backend + '/personas/generarReporte/' + $scope.ced + '/' + $scope.filt + '/' + $scope.ape + '/' + $scope.userLog, {responseType: 'arraybuffer'})
                            .success(function (data) {
                                var file = new Blob([data], {type: 'application/pdf'});
                                var fileURL = URL.createObjectURL(file);
                                window.open(fileURL);
                            });

                }

                $scope.limpiar = function () {
                    $scope.filterText = "";
                    $scope.apellido = "";
                    $scope.ci = "";
                };

                $scope.cargarNivel2 = function (n2, n3, n4, n5) {
                    $("#cateTwoMod").css("display", "none");
                    $("#cateTwoSMod").css("display", "none");
                    $scope.cate2Mod = [];
                    mapCateMod2 = new Map();
                    $scope.idCate2Mod = "";

                    $("#cateThreeMod").css("display", "none");
                    $("#cateThreeSMod").css("display", "none");
                    $scope.cate3Mod = [];
                    mapCateMod3 = new Map();
                    $scope.idCate3Mod = "";

                    $("#cateFourMod").css("display", "none");
                    $("#cateFourSMod").css("display", "none");
                    $scope.cate4Mod = [];
                    mapCateMod4 = new Map();
                    $scope.idCate4Mod = "";

                    $("#cateFiveMod").css("display", "none");
                    $("#cateFiveSMod").css("display", "none");
                    $scope.cate5Mod = [];
                    mapCateMod5 = new Map();
                    $scope.idCate5Mod = "";


                    UsuarioService.listarCategoriasFilter($("#primeraCategoriaMod").val()).success(function (data) {
                        $scope.cate2Mod = [];
                        var num = 0;
                        angular.forEach(data, function (value, key) {
                            num++;
                            if (parseInt(value.parent) === parseInt($("#primeraCategoriaMod").val())) {
                                $scope.cate2Mod.push(value.name);
                                mapCateMod2.set(value.name, value.id);
                            }
                            if (num === data.length) {
                                $scope.idCate2Mod = JSON.parse(JSON.stringify(mapUsuariosJSON.get(n2))).name;

                                if (n3 > 0) {
                                    $scope.cargarNivel3(n3, n4, n5);
                                }
                            }
                        });
                        $("#cateTwoMod").css("display", "inline");
                        $("#cateTwoSMod").css("display", "inline");

                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }
                $scope.cargarNivel3 = function (n3, n4, n5) {
                    $("#cateThreeMod").css("display", "none");
                    $("#cateThreeSMod").css("display", "none");
                    $scope.cate3Mod = [];
                    mapCateMod3 = new Map();
                    $scope.idCate3Mod = "";

                    $("#cateFourMod").css("display", "none");
                    $("#cateFourSMod").css("display", "none");
                    $scope.cate4Mod = [];
                    mapCateMod4 = new Map();
                    $scope.idCate4Mod = "";

                    $("#cateFiveMod").css("display", "none");
                    $("#cateFiveSMod").css("display", "none");
                    $scope.cate5Mod = [];
                    mapCateMod5 = new Map();
                    $scope.idCate5Mod = "";

                    var num = 0;
                    UsuarioService.listarCategoriasFilter(mapCateMod2.get($scope.idCate2Mod)).success(function (data) {
                        $scope.cate3Mod = [];
                        angular.forEach(data, function (value, key) {
                            num++;
                            if (parseInt(value.parent) === parseInt(mapCateMod2.get($scope.idCate2Mod))) {
                                $scope.cate3Mod.push(value.name);
                                mapCateMod3.set(value.name, value.id);
                            }
                            if (num === data.length) {
                                $scope.idCate3Mod = JSON.parse(JSON.stringify(mapUsuariosJSON.get(n3))).name;

                                if (n4 > 0) {
                                    $scope.cargarNivel4(n4, n5);
                                }
                            }
                        });
                        $("#cateThreeMod").css("display", "inline");
                        $("#cateThreeSMod").css("display", "inline");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }
                $scope.cargarNivel4 = function (n4, n5) {
                    $("#cateFourMod").css("display", "none");
                    $("#cateFourSMod").css("display", "none");
                    $scope.cate4Mod = [];
                    mapCateMod4 = new Map();
                    $scope.idCate4Mod = "";

                    $("#cateFiveMod").css("display", "none");
                    $("#cateFiveSMod").css("display", "none");
                    $scope.cate5Mod = [];
                    mapCateMod5 = new Map();
                    $scope.idCate5Mod = "";

                    var num = 0;
                    UsuarioService.listarCategoriasFilter(mapCateMod3.get($scope.idCate3Mod)).success(function (data) {
                        $scope.cate4Mod = [];
                        angular.forEach(data, function (value, key) {
                            num++;
                            if (parseInt(value.parent) === parseInt(mapCateMod3.get($scope.idCate3Mod)) && value.visible === 1) {
                                $scope.cate4Mod.push(value.name);
                                mapCateMod4.set(value.name, value.id);
                            }
                            if (num === data.length) {
                                $scope.idCate4Mod = JSON.parse(JSON.stringify(mapUsuariosJSON.get(n4))).name;

                                if (n5 > 0) {
                                    $scope.cargarNivel4(n5);
                                }
                            }
                            $("#cateFourMod").css("display", "inline");
                            $("#cateFourSMod").css("display", "inline");
                        });

                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }
                $scope.cargarNivel5 = function (n5) {
                    $("#cateFiveMod").css("display", "none");
                    $("#cateFiveSMod").css("display", "none");
                    $scope.cate5Mod = [];
                    mapCateMod5 = new Map();
                    $scope.idCate5Mod = "";

                    var num = 0;
                    UsuarioService.listarCategoriasFilter(mapCateMod4.get($scope.idCate4Mod)).success(function (data) {
                        $scope.cate5Mod = [];
                        angular.forEach(data, function (value, key) {
                            num++;
                            if (parseInt(value.parent) === parseInt(mapCateMod4.get($scope.idCate4Mod)) && value.visible === 1) {
                                $scope.cate5Mod.push(value.name);
                                mapCateMod5.set(value.name, value.id);
                            }
                            if (num === data.length) {
                                $scope.idCate5Mod = JSON.parse(JSON.stringify(mapUsuariosJSON.get(n5))).name;
                            }
                            $("#cateFiveMod").css("display", "inline");
                            $("#cateFiveSMod").css("display", "inline");
                        });

                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }

                $scope.buscarPadreHastaFacuOPost = function (categoryId) {
                    var cate5 = "";
                    var cate4 = "";
                    var cate3 = "";
                    var cate2 = "";
                    var cate1 = "";

                    var num = 0;

                    if (mapUsuarios.get(categoryId) !== undefined) {
                        if (mapUsuarios.get(categoryId) + "".toUpperCase() !== "FACULTADES" || mapUsuarios.get(categoryId) + "".toUpperCase() !== "POSTGRADO") {
                            cate5 = categoryId;
                            num = num + 1;

                            if (mapUsuariosParent.get(cate5) !== undefined) {
                                if (mapUsuarios.get(mapUsuariosParent.get(cate5)) + "".toUpperCase() !== "FACULTADES" || mapUsuarios.get(mapUsuariosParent.get(cate5)) + "".toUpperCase() !== "POSTGRADO") {
                                    cate4 = mapUsuariosParent.get(cate5);
                                    if (cate4 > 0) {
                                        num = num + 1;
                                    }

                                    if (mapUsuariosParent.get(cate4) !== undefined) {
                                        if (mapUsuarios.get(mapUsuariosParent.get(cate4)) + "".toUpperCase() !== "FACULTADES" || mapUsuarios.get(mapUsuariosParent.get(cate4)) + "".toUpperCase() !== "POSTGRADO") {
                                            cate3 = mapUsuariosParent.get(cate4);
                                            if (cate3 > 0) {
                                                num = num + 1;
                                            }

                                            if (mapUsuariosParent.get(cate3) !== undefined) {
                                                if (mapUsuarios.get(mapUsuariosParent.get(cate3)) + "".toUpperCase() !== "FACULTADES" || mapUsuarios.get(mapUsuariosParent.get(cate3)) + "".toUpperCase() !== "POSTGRADO") {
                                                    cate2 = mapUsuariosParent.get(cate3);
                                                    if (cate2 > 0) {
                                                        num = num + 1;
                                                    }
                                                    if (mapUsuariosParent.get(cate2) !== undefined) {
                                                        if (mapUsuarios.get(mapUsuariosParent.get(cate2)) + "".toUpperCase() !== "FACULTADES" || mapUsuarios.get(mapUsuariosParent.get(cate2)) + "".toUpperCase() !== "POSTGRADO") {
                                                            cate1 = mapUsuariosParent.get(cate2);
                                                            if (cate1 > 0) {
                                                                num = num + 1;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (num === 1) {
                        console.log(mapUsuarios.get(cate5))
                        $("#primeraCategoriaMod").val(cate5);
                    } else if (num === 2) {
                        console.log(mapUsuarios.get(cate5) + " - " + mapUsuarios.get(cate4))
                        $("#primeraCategoriaMod").val(cate4);
                        $scope.cargarNivel2(cate5, 0, 0, 0);
                    } else if (num === 3) {
                        console.log(mapUsuarios.get(cate5) + " - " + mapUsuarios.get(cate4) + " - " + mapUsuarios.get(cate3))
                        $("#primeraCategoriaMod").val(cate3);
                        $scope.cargarNivel2(cate4, cate5, 0, 0);
                    } else if (num === 4) {
                        console.log(mapUsuarios.get(cate5) + " - " + mapUsuarios.get(cate4) + " - " + mapUsuarios.get(cate3) + " - " + mapUsuarios.get(cate2))
                        $("#primeraCategoriaMod").val(cate2);
                        $scope.cargarNivel2(cate3, cate4, cate5, 0);
                    } else if (num === 5) {
                        console.log(mapUsuarios.get(cate5) + " - " + mapUsuarios.get(cate4) + " - " + mapUsuarios.get(cate3) + " - " + mapUsuarios.get(cate2) + " - " + mapUsuarios.get(cate1))
                        $("#primeraCategoriaMod").val(cate1);
                        $scope.cargarNivel2(cate2, cate3, cate4, cate5);
                    }
                };

                $scope.registrar = function () {
                    if ($scope.personaAgregar.nombre === "" || $scope.personaAgregar.nombre === undefined || $scope.personaAgregar.nombre === null ||
                            $scope.personaAgregar.apellido === "" || $scope.personaAgregar.apellido === undefined || $scope.personaAgregar.apellido === null ||
                            $scope.personaAgregar.email === "" || $scope.personaAgregar.email === undefined || $scope.personaAgregar.email === null ||
                            $scope.personaAgregar.usuario === "" || $scope.personaAgregar.usuario === undefined || $scope.personaAgregar.usuario === null ||
                            $scope.personaAgregar.pass === "" || $scope.personaAgregar.pass === undefined || $scope.personaAgregar.pass === null ||
                            $scope.personaAgregar.repeti === "" || $scope.personaAgregar.repeti === undefined || $scope.personaAgregar.repeti === null
                            ) {
                        swal("Mensaje del Sistema!", "Todos los campos son requeridos.", "error");
                    } else if ($scope.personaAgregar.pass !== $scope.personaAgregar.repeti) {
                        swal("Mensaje del Sistema!", "Los campos contraseña y repetición no coinciden.", "error");
                    } else {

                        var urlParameter = "users[0][username]=" + $scope.personaAgregar.usuario + "&users[0][email]=" + $scope.personaAgregar.email
                                + "&users[0][lastname]=" + $scope.personaAgregar.nombre + "&users[0][password]=" + $scope.personaAgregar.pass + "&users[0][firstname]=" + $scope.personaAgregar.apellido;

                        UsuarioService.crear(urlParameter).success(function (data) {
                            console.log("data -> ", data)
                            if (data.exception !== "invalid_parameter_exception" && data.exception !== "moodle_exception") {
                                swal("Mensaje del Sistema!", "Datos Agregados exitosamente", "success");
                                $('#modalAgregar').modal('hide');
                                $scope.listar();
                            } else if (data.exception === "moodle_exception") {
                                swal("Mensaje del Sistema!", "Datos no Agregados, " + data.message, "error");
                            } else {
                                swal("Mensaje del Sistema!", "Datos no Agregados, verifique que no exista uno similar", "error");
                            }
                        }).error(function (e) {
                            swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                        });
                    }
                };
                $scope.agregar = function () {
                    $scope.personaAgregar.nombre = "";
                    $scope.personaAgregar.corto = "";
                    $("#primeraCategoria").val("0");
                    $scope.personaAgregar.cate2 = [];
                    $scope.personaAgregar.cate3 = [];
                    $scope.personaAgregar.cate4 = [];
                    $scope.personaAgregar.visible = "";
                    $scope.personaAgregar.inicio = "";
                    $scope.personaAgregar.fin = "";

                    $("#cateTwo").css("display", "none");
                    $("#cateTwoS").css("display", "none");

                    $("#cateThree").css("display", "none");
                    $("#cateThreeS").css("display", "none");

                    $("#cateFour").css("display", "none");
                    $("#cateFourS").css("display", "none");


                };
                $scope.atras = function () {
                    location.reload();
                };
                $scope.actualizar = function () {
                    if ($scope.personaModificar.nombre === "" || $scope.personaModificar.nombre === undefined || $scope.personaModificar.nombre === null ||
                            $scope.personaModificar.apellido === "" || $scope.personaModificar.apellido === undefined || $scope.personaModificar.apellido === null ||
                            $scope.personaModificar.email === "" || $scope.personaModificar.email === undefined || $scope.personaModificar.email === null ||
                            $scope.personaModificar.usuario === "" || $scope.personaModificar.usuario === undefined || $scope.personaModificar.usuario === null ||
                            $scope.personaModificar.pass === "" || $scope.personaModificar.pass === undefined || $scope.personaModificar.pass === null ||
                            $scope.personaModificar.repeti === "" || $scope.personaModificar.repeti === undefined || $scope.personaModificar.repeti === null
                            ) {
                        swal("Mensaje del Sistema!", "Todos los campos son requeridos.", "error");
                    } else {
                        var urlParameter = "users[0][id]=" + $scope.personaModificar.id + "&users[0][username]=" + $scope.personaModificar.usuario + "&users[0][email]=" + $scope.personaModificar.email
                                + "&users[0][lastname]=" + $scope.personaModificar.apellido + "&users[0][password]=" + $scope.personaModificar.pass + "&users[0][firstname]=" + $scope.personaModificar.nombre;

                        UsuarioService.actualizar(urlParameter).success(function (data) {
                            console.log("data -> ", data)
                            if (data === null) {
                                swal("Mensaje del Sistema!", "Datos Actualizados exitosamente", "success");
                                $('#modalModificar').modal('hide');
                                $scope.listar();
                            } else if (data.exception === "moodle_exception") {
                                swal("Mensaje del Sistema!", "Datos no Agregados, " + data.message, "error");
                            } else {
                                swal("Mensaje del Sistema!", "Datos no Agregados, verifique que no exista uno similar", "error");
                            }
                        }).error(function (e) {
                            swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                        });
                    }
                };

                $scope.modificar = function (personas) {
                    $scope.personaModificar.id = personas.id;
                    $scope.personaModificar.nombre = personas.firstname;
                    $scope.personaModificar.apellido = personas.lastname;
                    $scope.personaModificar.email = personas.email;
                    $scope.personaModificar.usuario = personas.username;
                    $scope.personaModificar.pass = personas.password;
                    $scope.personaModificar.repeti = personas.password;

                    $("#modalModificar").modal("show");
                };

                $scope.eliminar = function (roles) {
                    swal({
                        title: "Mensaje del Sistema",
                        text: "Seguro que quiere eliminar la Persona " + roles.nombrePersona + " " + roles.apellidoPersona + "?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Si",
                        cancelButtonText: "No",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                            function (isConfirm) {
                                if (isConfirm) {
                                    UsuarioService.eliminar(roles.idPersona).success(function (data) {
                                        if (data === true) {
                                            swal('Eliminado!',
                                                    'Haz eliminado exitosamente.',
                                                    'success'
                                                    );
                                            $scope.listar();
                                        } else {
                                            swal("Mensaje del Sistema!", "No se pudo a la Persona, verifique existencia en Usuarios", "error");
                                        }
                                    }).error(function (e) {
                                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                                    });
                                } else {
                                    swal("Mensaje del Sistema", "Haz cancelado la operación!", "error");
                                }
                            });
                };

//                $scope.buscar();
                $scope.listar();
            }]);