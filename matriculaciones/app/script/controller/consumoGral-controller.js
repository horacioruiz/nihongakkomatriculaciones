'use strict'
app
        .controller('listConsumoGralCtrl', ['$state', '$scope', 'config', 'EdificioService', 'NodoService', '$cookies', '$base64', 'ConsumoGralService', 'PersonaService', 'TipoEdificioService', 'CiudadService', '$http', '$rootScope', '$filter', function ($state, $scope, config, EdificioService, NodoService, $cookies, $base64, ConsumoGralService, PersonaService, TipoEdificioService, CiudadService, $http, $rootScope, $filter) {
                $scope.consumoGral = {};
                $scope.consumoGralAgregar = {};
                $scope.consumoGralModificar = {};
                $scope.nodoPinAgregar = {};
                $scope.consumoGralAgregar.andeCategoria = {};
                $scope.consumoGralAgregar.tipoEdificio = {};
                $scope.consumoGralAgregar.persona = {};
                $scope.consumoGralAgregar.ciudad = {};
                $scope.consumoGralModificar.andeCategoria = {};
                $scope.consumoGralModificar.tipoEdificio = {};
                $scope.consumoGralModificar.persona = {};
                $scope.consumoGralModificar.ciudad = {};
                $scope.aModificar = "";
                $scope.fechaDesde = "";
                $scope.fechaHasta = "";
                $scope.dataY = [];
                $scope.dataX = [];
                var valorMin = 9999;
                var valorMax = 0;
                $("#paginaActual").val("1");
                $scope.totalItems = 0;
                $scope.currentPage = 0;
                $scope.pageChangedFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };
                $scope.pageChangedSinFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };
                var myMap = new Map();
                var myMapNodo = new Map();
                $scope.listar = function () {
                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    ConsumoGralService.fetch(config.cantPaginacion, offSet).success(function (data) {
                        $scope.consumoGral = data;
                        $scope.paginar();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };
                $scope.limpiar = function () {
                    document.getElementById("fecha").valueAsDate = null;
                    $scope.filterText = "";
                };
                $scope.generar = function () {
                    $("#menuInicial").css("display", "none");
                    $("#addChart").css("display", "inline");
                    $("#chart01").css("display", "none");
                    $("#edificioPlaca").css("display", "none");
                    $("#DependenciaComponente").css("display", "none");
                    $scope.nestedItemsLevel1 = [];
                    $scope.idNodo = 0;
                    $scope.fechaDesde = "";
                    $scope.fechaHasta = "";
                    NodoService.listar().success(function (d) {
                        angular.forEach(d, function (value, key) {
                            $scope.nestedItemsLevel1.push(value.descriNodo);
                            myMapNodo.set(value.descriNodo, value.idNodo);
                        });
                    });
                };
                $scope.onChange = function () {
                    $("#edificioPlaca").css("display", "none");
                    $("#DependenciaComponente").css("display", "none");
                    NodoService.listarPorId(myMapNodo.get($scope.idNodo)).success(function (data) {
                        $scope.nodoPinAgregar.descriEdificio = data.edificio.descriEdificio;
                        $scope.nodoPinAgregar.descriPlaca = data.placa.descriPlaca;
                        $scope.nodoPinAgregar.descriTipoPlaca = data.placa.tipoPlaca.descriTipo;
                        $scope.nodoPinAgregar.descriDependencia = data.dependencia.descriDependencia;
                        $scope.nodoPinAgregar.descriComponente = data.componente.descriComponente;
                        $("#edificioPlaca").fadeIn("slow");
                        $("#DependenciaComponente").fadeIn("slow");
                    });
                };
//FUNCIONAL 2
                $scope.generarGrafico = function () {
                    $scope.dataX = [];
                    $scope.dataY = [];
                    if (!isNaN(new Date($scope.fechaDesde).getTime())) {
                        valorMin = 9999;
                        valorMax = 0;
                        $("#chart01").css("display", "none");
//                        $("#edificioPlaca").css("display", "none");
//                        $("#DependenciaComponente").css("display", "none");
                        var date = new Date($('#fechaDesde').val());
                        var day = date.getDate() + 1;
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();
                        var fecha = [year, ("0" + month).slice(-2), ("0" + day).slice(-2)].join('-');
                        $scope.fec = fecha;
                        if ($scope.fec === "NaN-aN-aN") {
                            $scope.fec = null;
                        }
                        ConsumoGralService.listarPorIdNodoYFecha(myMapNodo.get($scope.idNodo), $scope.fec).success(function (data) {
                            if (data.length === 0) {
                                swal("Mensaje del Sistema!", "No se encontraron resultados de la búsqueda.", "error");
                            } else {
                                var tamanho = data.length;
                                var count = 0;
                                angular.forEach(data, function (value, key) {
                                    if (valorMin > value.corrienteConsu) {
                                        valorMin = value.corrienteConsu;
                                    }
                                    if (valorMax < value.corrienteConsu) {
                                        valorMax = value.corrienteConsu;
                                    }
                                    if (valorMin === valorMax) {
                                        valorMin = 0;
                                    }
                                    console.log(count + ") $scope.corrienteConsu -> " + value.corrienteConsu);
//                                $scope.dataY.push(parseInt(value.corrienteConsu));
                                    $scope.sample = $filter('date')(value.consumoFecha, "HH:mm");
                                    $scope.dataX.push($scope.sample);
//                                final
                                    $scope.dataX.push(parseFloat(value.corrienteConsu));
                                    $scope.dataY.push($scope.dataX);
                                    $scope.dataX = [];
//                                final
                                    count++;
                                    if (count === tamanho) {
                                        $scope.generarlo();
                                    }
                                });
                            }
                        }).error(function (e) {
                            swal("Mensaje del Sistema!", "No se pudo conectar con el Servidor..", "error");
                        });
                    } else if ($scope.idNodo === 0) {
                        swal("Mensaje del Sistema!", "El campo Nodo no debe quedar vacío", "error");
                    } else {
                        swal("Mensaje del Sistema!", "El campo Fecha no debe quedar vacío", "error");
                    }

                };
                $scope.generarlo = function () {
                    console.log("MIN: " + valorMin + " MAX: " + valorMax + "->> " + "'" + valorMin + ":" + valorMax + ":" + (valorMax / 10) + "'", );
                    if ($scope.idEdificio === 0) {
                        swal("Mensaje del Sistema!", "El campo Edificio no deben quedar vacío.", "error");
                    } else if (!isNaN(new Date($scope.fechaDesde).getTime())) {
                        $scope.myJson = {
                            type: 'line',
                            title: {
                                text: 'Gráfico generado',
                                align: 'left',
                                fontColor: '#FFFFFF'
                            },
                            backgroundColor: "#30a5ff",
                            plot: {
                                lineWidth: "2px",
                                lineColor: "#FFFFFF",
                                aspect: "spline",
                                marker: {
                                    visible: true
                                }
                            },
                            plotarea: {
                            },
                            crosshairX: {
                                lineStyle: 'dashed',
                                lineWidth: 2,
                                lineColor: '#FFFFFF',
                                plotLabel: {
                                    visible: false
                                },
                                marker: {
                                    type: 'triangle',
                                    size: 5,
                                    visible: true
                                }
                            },
                            preview: {
                                live: true
                            },
                            scaleY: {
                                values: "'" + valorMin + ":" + Math.ceil(valorMax) + ":" + ((Math.ceil(valorMax)) / 10) + "'",
                                lineColor: "#000000",
                                lineWidth: "1px",
                                tick: {
                                    lineColor: "white",
                                    lineWidth: "1px"
                                },
                                guide: {
                                    lineStyle: "solid",
                                    lineColor: "#249178"
                                },
                                item: {
                                    fontColor: "white"
                                }
                            },
                            scaleX: {
                                zooming: true,
                                zoomTo: [0, 10],
                                lineWidth: "1px",
                                lineColor: '#000000',
                                itemsOverlap: true,
                                tick: {
                                    lineColor: '#FFFFFF',
                                    alpha: 1,
                                    lineWidth: 2
                                },
                                item: {
                                    fontColor: "#FFFFFF"
                                },
                                guide: {
                                    visible: false
                                },
                                values: $scope.dataX
                            },
                            'scale-y-2': {
                            },
                            series: [
                                {
                                    values: $scope.dataY
                                }]
                        };
//                        $scope.myJson = {
//
//                            "type": "line",
//                            title: {
//                                text: 'Gráfico generado',
//                                align: 'left',
//                                fontColor: '#FFFFFF'
//                            },
//                            backgroundColor: "#30a5ff",
//                            plot: {
//                                lineWidth: "2px",
//                                lineColor: "#FFFFFF",
//                                aspect: "spline",
//                                marker: {
//                                    visible: true
//                                }
//                            },
//                            plotarea: {
//                            },
//                            crosshairX: {
//                                lineStyle: 'dashed',
//                                lineWidth: 2,
//                                lineColor: '#FFFFFF',
//                                plotLabel: {
//                                    visible: false
//                                },
//                                marker: {
//                                    type: 'triangle',
//                                    size: 5,
//                                    visible: true
//                                }
//                            },
//                            preview: {
//                                live: true
//                            },
//
////                            "scale-x": {
////                                "min-value": "1457101800000",
////                                "max-value": "1457125200000",
////                                "step": "30minute",
////                                "transform": {
////                                    "type": "date"
////                                },
////                                "label": {
////                                    "text": "Trading Day"
////                                },
////                                "item": {
////                                    "font-size": 10
////                                },
////                                "max-items": 14
////                            },
//                            scaleX: {
//                                zooming: true,
//                                zoomTo: [0, 30],
//                                lineWidth: "1px",
//                                lineColor: '#000000',
//                                itemsOverlap: true,
//                                tick: {
//                                    lineColor: '#FFFFFF',
//                                    alpha: 1,
//                                    lineWidth: 2
//                                },
//                                item: {
//                                    fontColor: "#FFFFFF"
//                                },
//                                guide: {
//                                    visible: false
//                                }
//                            },
////                            "scale-y": {
////                                "values": "30:34:1",
////                                "format": "$%v",
////                                "label": {
////                                    "text": "Price"
////                                },
////                                "item": {
////                                    "font-size": 10
////                                },
////                                "guide": {
////                                    "line-style": "solid"
////                                }
////                            },
//                            scaleY: {
////                                "values": "0:50:10",
//                                values: "'" + valorMin + ":" + valorMax + ":" + (valorMax / 10) + "'",
//                                lineColor: "#000000",
//                                lineWidth: "1px",
//                                tick: {
//                                    lineColor: "white",
//                                    lineWidth: "1px"
//                                },
//                                guide: {
//                                    lineStyle: "solid",
//                                    lineColor: "#249178"
//                                },
//                                item: {
//                                    fontColor: "white"
//                                }
//                            },
//                            "series": [
//                                {
//                                    values: $scope.dataY
////                                    "values": [
////                                        ['03/04/2016', 30.34], //03/04/2016 at 9:30 a.m. EST (which is 14:30 in GMT)
////                                        ['04/04/2016', 31.30], //10:00 a.m.
////                                        ['05/04/2016', 30.95], //10:30 a.m.
////                                        ['06/04/2016', 30.99], //11:00 a.m.
////                                        ['0/04/2016', 32.33], //11:30 a.m.
////                                        ['07/04/2016', 33.34], //12:00 p.m.
////                                        ['08/04/2016', 33.01], //12:30 p.m.
////                                        ['09/04/2016', 34], //1:00 p.m.
////                                        ['10/04/2016', 33.64], //1:30 p.m.
////                                        ['11/04/2016', 32.59], //2:00 p.m.
////                                        ['12/04/2016', 32.60], //2:30 p.m.
////                                        ['01/06/2016', 31.99], //3:00 p.m.
////                                        ['02/06/2016', 31.14], //3:30 p.m.
////                                        ['03/06/2016', 32.34] //at 4:00 p.m. EST (which is 21:00 GMT)
////                                    ]
//                                }
//                            ]
//                        };
                        $("#chart01").fadeIn("slow");
                    } else {
                        swal("Mensaje del Sistema!", "El campo Fecha no debe quedar vacío", "error");
                    }
                };
                $scope.atrasite = function () {
                    $("#addChart").css("display", "none");
                    $("#menuInicial").css("display", "inline");
                };
                $scope.paginar = function () {
                    ConsumoGralService.count().success(function (data) {
                        $scope.catidad = data;
                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());
                        $("#paginacionFiltro").css("display", "none");
                        $("#paginacionSinFiltro").css("display", "inline");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };
                $rootScope.$on("$locationChangeStart", function (event, data) {
                    var pag = parseInt(data.replace("http://" + config.ipFront + ":8017/menu.html#/:pagina=", ""));
                    if (!isNaN(pag)) {
                        $("#paginaActual").val(pag);
                        $scope.buscarSegunPagina();
                    }
                });
                $scope.paginarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.fecha === "" || $scope.fecha === undefined) {
                        $scope.fec = null;
                    } else {
                        var date = new Date($('#fecha').val());
                        var day = date.getDate() + 1;
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();
                        var fecha = [year, ("0" + month).slice(-2), ("0" + day).slice(-2)].join('-');
                        $scope.fec = fecha;
                        if ($scope.fec === "NaN-aN-aN") {
                            $scope.fec = null;
                        }
                    }

                    ConsumoGralService.countFiltro($scope.filt, $scope.fec).success(function (data) {
                        $scope.catidad = data;
                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());
                        $("#paginacionFiltro").css("display", "inline");
                        $("#paginacionSinFiltro").css("display", "none");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };
                $scope.listarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.fecha === "" || $scope.fecha === undefined) {
                        $scope.fec = null;
                    } else {
                        var date = new Date($('#fecha').val());
                        var day = date.getDate() + 1;
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();
                        var fecha = [year, ("0" + month).slice(-2), ("0" + day).slice(-2)].join('-');
                        $scope.fec = fecha;
                        if ($scope.fec === "NaN-aN-aN") {
                            $scope.fec = null;
                        }
                    }

                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    ConsumoGralService.fetchFiltro(config.cantPaginacion, offSet, $scope.filt, $scope.fec).success(function (data) {
                        $scope.consumoGral = data;
                        $scope.paginarConFiltro();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }

                $scope.generarReporte = function () {
                    $scope.jsonData = $cookies.getObject("usuarioLogueado");
                    $scope.userLog = $scope.jsonData.usuarioUsuario;
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.fecha === "" || $scope.fecha === undefined) {
                        $scope.fec = null;
                    } else {
                        var date = new Date($('#fecha').val());
                        var day = date.getDate() + 1;
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();
                        var fecha = [year, ("0" + month).slice(-2), ("0" + day).slice(-2)].join('-');
                        $scope.fec = fecha;
                        if ($scope.fec === "NaN-aN-aN") {
                            $scope.fec = null;
                        }
                    }
                    $http.get(config.backend + '/consumoGeneral/generarReporte/' + $scope.filt + '/' + $scope.fec + '/' + $scope.userLog, {responseType: 'arraybuffer'})
                            .success(function (data) {
                                var file = new Blob([data], {type: 'application/pdf'});
                                var fileURL = URL.createObjectURL(file);
                                window.open(fileURL);
                            });
                };
                $scope.buscar = function () {
                    $scope.consumoGral = [];
                    location.href = "http://" + config.ipFront + ":8017/menu.html#/:pagina=1";
                    if ($scope.filterText !== "" || $scope.fecha !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
                };
                $scope.buscarSegunPagina = function () {
                    $scope.consumoGral = [];
//                    location.href = "http://" + config.ipFront + ":8017/menu.html#/:pagina=1";
                    if ($scope.filterText !== "" || $scope.fecha !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
                };
                //FIN DE PAGINACION

                $scope.reporte = function () {
                    $scope.jsonData = $cookies.getObject("usuarioLogueado");
                    $scope.userLog = $scope.jsonData.usuarioUsuario;
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.fecha === "" || $scope.fecha === undefined) {
                        $scope.fec = null;
                    } else {
                        var date = new Date($('#fecha').val());
                        var day = date.getDate() + 1;
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();
                        var fecha = [year, ("0" + month).slice(-2), ("0" + day).slice(-2)].join('-');
                        $scope.fec = fecha;
                        if ($scope.fec === "NaN-aN-aN") {
                            $scope.fec = null;
                        }
                    }

                    $scope.result = {};
                    $http.get(config.backend + '/consumoGeneral/reportar/' + $scope.fec + '/' + $scope.filt + '/' + $scope.userLog, {responseType: 'arraybuffer'})
                            .success(function (data) {
                                var file = new Blob([data], {type: 'application/pdf'});
                                var fileURL = URL.createObjectURL(file);
                                window.open(fileURL);
                            });
                }

                $scope.$watch('filterText', function () {
                    $("#paginaActual").val("1");
                })
                $scope.$watch('fecha', function () {
                    $("#paginaActual").val("1");
                })
                $scope.atras = function () {
                    location.reload();
                };
                $scope.buscar();
            }])
        .controller('listPruebaConsumoGralCtrl', ['$state', '$scope', 'config', 'EdificioService', 'NodoService', 'UsuarioEdificioService', '$cookies', '$base64', 'ConsumoGralService', 'PersonaService', 'TipoEdificioService', 'CiudadService', '$http', '$rootScope', '$filter', function ($state, $scope, config, EdificioService, NodoService, UsuarioEdificioService, $cookies, $base64, ConsumoGralService, PersonaService, TipoEdificioService, CiudadService, $http, $rootScope, $filter) {
                $scope.jsonUsu = $cookies.getObject("usuarioLogueado");
                $scope.userLog = $scope.jsonUsu.usuarioUsuario;
                var myMapEdificio = new Map();
                $scope.agregar = {};
                $scope.agregar.edificio = {};
//                $scope.componente = "";
                $scope.idDato = 0;
                //CRONOMETRO

                var vueltas = 0;
                var cron;
                var sv_min = 0;
                var sv_hor = 0;
                var sv_seg = 0;
                var seg = document.getElementById('seg');
                var min = document.getElementById('min');
                var hor = document.getElementById('hor');
                var iniciado = false; //init status of cron

//                $("#btn_play").click(function () {
                $scope.iniciar = function () {
//                    alert("-->> " + myMapEdificio.get($scope.idEdificio) + " - " + $scope.idEdificio);
                    if (!iniciado) {
                        if ($scope.componente !== "" && $scope.idEdificio !== 0) {
                            $scope.agregar = {};
                            $scope.agregar.edificio = {};
                            EdificioService.listarPorDescri($scope.idEdificio).success(function (data) {
//                                alert(data.ipConsumo)
                                ConsumoGralService.arduinoIniciar(data.ipConsumo).success(function (d) {
                                    if (d.ESTADO === "ON") {
                                        $scope.agregar.edificio.idEdificio = myMapEdificio.get($scope.idEdificio);
                                        ConsumoGralService.crear($scope.agregar).success(function (data) {
                                            if (parseInt(data.idConsumo) !== 0) {
                                                $scope.idDato = data.idConsumo;
                                                $scope.finish = "";
                                                $scope.horaInicio = $filter('date')(new Date(data.consumoFecha), 'yyyy-MM-dd') + "  -  " + data.inicio;
                                                document.getElementById("btn_play").disabled = true;
                                                document.getElementById("btn_lap").disabled = false;
                                                iniciado = true;
                                                start_cron();
                                            }
                                        });
                                    } else {
                                        swal("Mensaje del Sistema!", "No se ha establecido comunicación con el módulo Arduino", "error");
                                    }
                                }).error(function (e) {
                                    swal("Mensaje del Sistema!", "ERROR: No se ha establecido comunicación con el módulo Arduino", "error");
                                });
                            });
                        } else {
                            swal("Mensaje del Sistema!", "Los campos no deben quedar vacío para iniciar el control de consumo", "error");
                        }
                    }
                }


                function start_cron() {
                }

                $("#btn_pause").click(function () {
                    clearInterval(cron);
                    iniciado = false;
                });
                $("#btn_stop").click(function () {
                    sv_min = 0;
                    sv_hor = 0;
                    sv_seg = 0;
                    seg.innerHTML = "00";
                    min.innerHTML = "00";
                    hor.innerHTML = "00";
                    clearInterval(cron);
                    iniciado = false;
                });
                $scope.pausar = function () {
                    clearInterval(cron);
                    iniciado = false;
                };
                $scope.limpiarCronometro = function () {
                    sv_min = 0;
                    sv_hor = 0;
                    sv_seg = 0;
                    seg.innerHTML = "00";
                    min.innerHTML = "00";
                    hor.innerHTML = "00";
                    clearInterval(cron);
                    iniciado = false;
                };
//                $("#btn_lap").click(function () {
                $scope.finalizar = function () {
//                    $scope.pausar();
                    $scope.potencia = 0;
                    if ($scope.idEdificio !== 0) {
//                    if (hor.innerHTML + ":" + min.innerHTML + ":" + seg.innerHTML !== "00:00:00") {
                        EdificioService.listarPorDescri($scope.idEdificio.toUpperCase()).success(function (data) {
                            ConsumoGralService.arduinoGetPotencia(data.ipConsumo).success(function (da) {
                                if (da.ESTADO !== " NAN") {
                                    $scope.potencia = da.ESTADO;
                                    ConsumoGralService.actualizarDatos($scope.idDato, da.ESTADO).success(function (data) {
                                        if (data !== "{}") {
                                            vueltas++;
                                            $scope.finish = ' FIN: ' + $filter('date')(new Date(data.consumoFecha), 'yyyy-MM-dd') + "  -  " + data.fin + "  -  Gs. " + Number(data.precio).toFixed(2);

                                            document.getElementById("btn_play").disabled = false;
                                            document.getElementById("btn_lap").disabled = true;
                                            iniciado = false;
                                        }
                                    });
                                }
                            });
                        });
                    }
                }
//                });
                function consola(msg) {
                    $("#log").prepend(msg);
                }

                $("#btn_clear").click(function () {
                    $("#log").html("");
                    vueltas = 0;
                });
                //FIN DEL CRONOMETRO

                $scope.atras = function () {
                    location.reload();
                };
                $scope.agregarEdificio = function () {
                    $scope.nestedItemsLevelEdificio = [];
                    $scope.idEdificio = 0;
                    UsuarioEdificioService.getByIdUsu($scope.jsonUsu.idUsuario).success(function (data) {
                        angular.forEach(data, function (value, key) {
                            $scope.nestedItemsLevelEdificio.push(value.edificio.descriEdificio);
                            myMapEdificio.set(value.edificio.descriEdificio, value.edificio.idEdificio);
                        });
                    });
                };
                $scope.onChangeEdificio = function () {
                    EdificioService.listarPorId(myMapEdificio.get($scope.idEdificio)).success(function (d) {
                        $scope.tipoEdificio = "Tipo de Edificio: " + d.tipoEdificio.descriTipoEdi;
                        if (d.idEdificio > 0) {
                            ConsumoGralService.listarPendientes(myMapEdificio.get($scope.idEdificio)).success(function (data) {
                                if (data.idConsumo === null) {
                                    $scope.horaInicio = "";
                                    $scope.finish = "";
                                    document.getElementById("btn_play").disabled = false;
                                    document.getElementById("btn_lap").disabled = true;
                                } else {
                                    $scope.idDato = data.idConsumo;
                                    $scope.finish = "";
                                    $scope.horaInicio = "Inicio: " + $filter('date')(new Date(data.consumoFecha), 'yyyy-MM-dd') + "  -  " + data.inicio;
                                    document.getElementById("btn_play").disabled = true;
                                    document.getElementById("btn_lap").disabled = false;
                                }
                            });
                        }
                    });
                };
                $scope.agregarEdificio();
            }]);