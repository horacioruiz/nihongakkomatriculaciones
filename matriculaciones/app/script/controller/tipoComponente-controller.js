'use strict'
app
        .controller('listTipoComponenteCtrl', ['$state', '$scope', '$http', 'config', 'TipoComponenteService', '$cookies', '$base64', function ($state, $scope, $http, config, TipoComponenteService, $cookies, $base64) {
//                console.log("HOLAAA");

                $scope.tipoComponente = {};
                $scope.tipoComponenteAgregar = {};
                $scope.tipoComponenteModificar = {};
                $("#paginaActual").val("1");
                $scope.totalItems = 0;
                $scope.currentPage = 0;
                $scope.pageChangedFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };
                $scope.pageChangedSinFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };
//                FIN DE PAGINACION

                $scope.listar = function () {
                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    TipoComponenteService.fetch(config.cantPaginacion, offSet).success(function (data) {
                        $scope.tipoComponente = data;
                        $scope.paginar();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $scope.paginar = function () {
                    TipoComponenteService.count().success(function (data) {
                        $scope.catidad = data;
                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "none");
                        $("#paginacionSinFiltro").css("display", "inline");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $scope.paginarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    TipoComponenteService.countFiltro($scope.filt).success(function (data) {
                        $scope.catidad = data;

                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "inline");
                        $("#paginacionSinFiltro").css("display", "none");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $scope.generarReporte = function () {
                    $scope.jsonData = $cookies.getObject("usuarioLogueado");
                    $scope.userLog = $scope.jsonData.usuarioUsuario;

                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    $http.get(config.backend + '/tipoComponente/generarReporte/' + $scope.filt + '/' + $scope.userLog, {responseType: 'arraybuffer'})
                            .success(function (data) {
                                var file = new Blob([data], {type: 'application/pdf'});
                                var fileURL = URL.createObjectURL(file);
                                window.open(fileURL);
                            });
                };
                $scope.listarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }

                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    TipoComponenteService.fetchFiltro(config.cantPaginacion, offSet, $scope.filt).success(function (data) {
                        $scope.tipoComponente = data;
                        $scope.paginarConFiltro();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }

                $scope.buscar = function () {
                    $scope.tipoComponente = [];
                    if ($scope.filterText !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
//                    }
                };
                $scope.buscarSegunPagina = function () {
                    $scope.tipoComponente = [];
                    if ($scope.filterText !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
                };
                $scope.registrar = function () {
                    if ($scope.tipoComponenteAgregar.descriTipoComp === "" || $scope.tipoComponenteAgregar.descriTipoComp === undefined || $scope.tipoComponenteAgregar.descriTipoComp === null) {
                        swal("Mensaje del Sistema!", "Los datos no deben quedar vacío", "error");
                    } else {
                        $scope.jsonUsu = $cookies.getObject("usuarioLogueado");
                        $scope.tipoComponenteAgregar.usuAltaTipoComp = $scope.jsonUsu.usuarioUsuario;
                        $scope.tipoComponenteAgregar.usuModTipoComp = $scope.jsonUsu.usuarioUsuario;
                        TipoComponenteService.crear($scope.tipoComponenteAgregar).success(function (data) {
                            if (data === true) {
                                swal("Mensaje del Sistema!", "Datos Agregados exitosamente", "success");
                                $('#modalAgregar').modal('hide');
                                $scope.listar();
                            } else {
                                swal("Mensaje del Sistema!", "Datos no Agregados", "error");
                            }
                        }).error(function (e) {
                            swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                        });
                    }
                };
                $scope.atras = function () {
                    location.reload();
                };
                $scope.agregar = function () {
                    $scope.tipoComponenteAgregar = {};
                };
                $scope.actualizar = function () {
                    if ($scope.tipoComponenteModificar.descriTipoComp === "" || $scope.tipoComponenteModificar.descriTipoComp === undefined || $scope.tipoComponenteModificar.descriTipoComp === null) {
                        swal("Mensaje del Sistema!", "Los datos no deben quedar vacío", "error");
                    } else {
                        $scope.jsonUsu = $cookies.getObject("usuarioLogueado");
                        $scope.tipoComponenteModificar.usuModTipoComp = $scope.jsonUsu.usuarioUsuario;
                        TipoComponenteService.actualizar($scope.tipoComponenteModificar).success(function (data) {
                            if (data === true) {
                                swal("Mensaje del Sistema!", "Datos Modificados exitosamente", "success");
                                $('#modalModificar').modal('hide');
                                $scope.listar();
                            } else {
                                swal("Mensaje del Sistema!", "Datos no Modificados", "error");
                            }
                        }).error(function (e) {
                            swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                        });
                    }
                };
                $scope.modificar = function (tipoComponente) {
                    $scope.tipoComponenteModificar.descriTipoComp = tipoComponente.descriTipoComp;
                    $scope.tipoComponenteModificar.idTipoComp = tipoComponente.idTipoComp;
                    $("#modalModificar").modal("show");
                };
//                $scope.eliminar = function (marc) {
//
//                    $scope.datoEliminar = {};
//                    $scope.datoEliminar.idTipoComp = marc.idTipoComp;
//                    var r = confirm("Seguro que quiere eliminar " + marc.descriTipoComp + "?");
//                    if (r === true) {
//                        TipoComponenteService.eliminar(marc.idTipoComp).success(function (data) {
////                        TipoComponenteService.modEliminar($scope.datoEliminar).success(function (data) {
//                            if (data === true) {
//                                swal("Mensaje del Sistema!", "Datos Eliminados exitosamente", "success");
//                                $scope.tipoComponenteModificar = {};
//                                $scope.listar();
//                            } else {
//                                swal("Mensaje del Sistema!", "No se pudo eliminar el Tipo Componente, verifique Dependencias", "error");
//                            }
//                        }).error(function (e) {
//                            swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
//                        });
//                    } else {
//                        swal('Cancelado',
//                                'Haz cancelado la eliminación',
//                                'error'
//                                );
//                    }
//
//                };
                $scope.eliminar = function (marc) {
                    $scope.datoEliminar = {};
                    $scope.datoEliminar.idTipoComp = marc.idTipoComp;
                    swal({
                        title: "Mensaje del Sistema",
                        text: "Seguro que quiere eliminar " + marc.descriTipoComp + "?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Si",
                        cancelButtonText: "No",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                            function (isConfirm) {
                                if (isConfirm) {
                                    TipoComponenteService.eliminar(marc.idTipoComp).success(function (data) {
                                        if (data === true) {
                                            swal("Mensaje del Sistema!", "Datos Eliminados exitosamente", "success");
                                            $scope.tipoComponenteModificar = {};
                                            $scope.listar();
                                        } else {
                                            swal("Mensaje del Sistema!", "No se pudo eliminar el Tipo Componente, verifique Dependencias", "error");
                                        }
                                    }).error(function (e) {
                                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                                    });
                                } else {
                                    swal("Mensaje del Sistema", "Haz cancelado la operación!", "error");
                                }
                            });
                };
                $scope.listar();
            }]);