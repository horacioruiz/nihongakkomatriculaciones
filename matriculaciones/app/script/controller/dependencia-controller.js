'use strict'
app
        .controller('listDependenciaCtrl', ['$state', '$scope', 'config', 'DependenciaService', '$cookies', '$base64', '$http', function ($state, $scope, config, DependenciaService, $cookies, $base64, $http) {
//                console.log("HOLAAA");

                $scope.dependencia = {};
                $scope.dependenciaAgregar = {};
                $scope.dependenciaModificar = {};

                $("#paginaActual").val("1");
                $scope.totalItems = 0;
                $scope.currentPage = 0;
                $scope.pageChangedFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };
                $scope.pageChangedSinFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };
//                FIN DE PAGINACION

                $scope.listar = function () {
                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    DependenciaService.fetch(config.cantPaginacion, offSet).success(function (data) {
                        $scope.dependencia = data;
                        $scope.paginar();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $scope.paginar = function () {
                    DependenciaService.count().success(function (data) {
                        $scope.catidad = data;
                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "none");
                        $("#paginacionSinFiltro").css("display", "inline");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $scope.paginarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    DependenciaService.countFiltro($scope.filt).success(function (data) {
                        $scope.catidad = data;

                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "inline");
                        $("#paginacionSinFiltro").css("display", "none");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };
                $scope.listarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }

                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    DependenciaService.fetchFiltro(config.cantPaginacion, offSet, $scope.filt).success(function (data) {
                        $scope.dependencia = data;
                        $scope.paginarConFiltro();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };
                
                $scope.generarReporte = function () {
                    $scope.jsonData = $cookies.getObject("usuarioLogueado");
                    $scope.userLog = $scope.jsonData.usuarioUsuario;

                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }

                    $http.get(config.backend + '/dependencia/generarReporte/' + $scope.filt + '/' + $scope.userLog, {responseType: 'arraybuffer'})
                            .success(function (data) {
                                var file = new Blob([data], {type: 'application/pdf'});
                                var fileURL = URL.createObjectURL(file);
                                window.open(fileURL);
                            });
                };

                $scope.buscar = function () {
                    $scope.dependencia = [];
                    if ($scope.filterText !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
//                    }
                };
                $scope.buscarSegunPagina = function () {
                    $scope.dependencia = [];
                    if ($scope.filterText !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
                };

                $scope.registrar = function () {
//                    alert($scope.rolAgregar.descripcionRolAgregar);
                    if ($scope.dependenciaAgregar.descriDependencia === "" || $scope.dependenciaAgregar.descriDependencia === undefined || $scope.dependenciaAgregar.descriDependencia === null) {
                        swal("Mensaje del Sistema!", "Los datos no deben quedar vacío", "error");
                    } else {
                        $scope.jsonUsu = $cookies.getObject("usuarioLogueado");
                        $scope.dependenciaAgregar.usuAltaDepe = $scope.jsonUsu.usuarioUsuario;
                        $scope.dependenciaAgregar.usuModDepe = $scope.jsonUsu.usuarioUsuario;
                        DependenciaService.crear($scope.dependenciaAgregar).success(function (data) {
                            if (data === true) {
                                swal("Mensaje del Sistema!", "Datos Agregados exitosamente", "success");
                                $('#modalAgregar').modal('hide');
//                            $scope.dependenciaAgregar = {};
                                $scope.listar();
                            } else {
                                swal("Mensaje del Sistema!", "Datos no Agregados", "error");
                            }
                        }).error(function (e) {
                            swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                        });
                    }
                };

                $scope.agregar = function () {
                    $scope.dependenciaAgregar = {};
                };

                $scope.atras = function () {
                    location.reload();
                };

                $scope.actualizar = function () {
//                    alert($scope.rolAgregar.descripcionRolAgregar);
                    if ($scope.dependenciaModificar.descriDependencia === "" || $scope.dependenciaModificar.descriDependencia === undefined || $scope.dependenciaModificar.descriDependencia === null) {
                        swal("Mensaje del Sistema!", "Los datos no deben quedar vacío", "error");
                    } else {
                        $scope.jsonUsu = $cookies.getObject("usuarioLogueado");
                        $scope.dependenciaModificar.usuModDepe = $scope.jsonUsu.usuarioUsuario;
                        DependenciaService.actualizar($scope.dependenciaModificar).success(function (data) {
                            if (data === true) {
                                swal("Mensaje del Sistema!", "Datos Modificados exitosamente", "success");
                                $('#modalModificar').modal('hide');
//                            $scope.dependenciaModificar = {};
                                $scope.listar();
                            } else {
                                swal("Mensaje del Sistema!", "Datos no Modificados", "error");
                            }
                        }).error(function (e) {
                            swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                        });
                    }
                };

                $scope.modificar = function (dependencia) {
                    $scope.dependenciaModificar.descriDependencia = dependencia.descriDependencia;
                    $scope.dependenciaModificar.idDependencia = dependencia.idDependencia;
                    $("#modalModificar").modal("show");
                };
//                $scope.eliminar = function (marc) {
//
//                    $scope.datoEliminar = {};
//                    $scope.datoEliminar.idDependencia = marc.idDependencia;
//
//                    var r = confirm("Seguro que quiere eliminar " + marc.descriDependencia + "?");
//                    if (r === true) {
//                        DependenciaService.modEliminar($scope.datoEliminar).success(function (data) {
//                            if (data === true) {
//                                swal("Mensaje del Sistema!", "Datos Modificados exitosamente", "success");
////                                $('#modalModificar').modal('hide');
//                                $scope.dependenciaModificar = {};
//                                $scope.listar();
//                            } else {
//                                swal("Mensaje del Sistema!", "Datos no Modificados", "error");
//                            }
//                        }).error(function (e) {
//                            swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
//                        });
//                    } else {
//                        swal('Cancelado',
//                                'Haz cancelado la eliminación',
//                                'error'
//                                );
//                    }
//                };

                $scope.eliminar = function (marc) {
                    $scope.datoEliminar = {};
                    $scope.datoEliminar.idDependencia = marc.idDependencia;
                    $scope.jsonUsu = $cookies.getObject("usuarioLogueado");
                    $scope.datoEliminar.usuModDepe = $scope.jsonUsu.usuarioUsuario;
                    swal({
                        title: "Mensaje del Sistema",
                        text: "Seguro que quiere eliminar " + marc.descriDependencia + "?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Si",
                        cancelButtonText: "No",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                            function (isConfirm) {
                                if (isConfirm) {
                                    DependenciaService.eliminar(marc.idDependencia).success(function (data) {
//                                    DependenciaService.modEliminar($scope.datoEliminar).success(function (data) {
                                        if (data === true) {
                                            swal("Mensaje del Sistema!", "Datos Modificados exitosamente", "success");
//                                $('#modalModificar').modal('hide');
                                            $scope.dependenciaModificar = {};
                                            $scope.listar();
                                        } else {
                                            swal("Mensaje del Sistema!", "No se pudo eliminar el Dependencia, verifique que no haya referencias", "error");
                                        }
                                    }).error(function (e) {
                                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                                    });
                                } else {
                                    swal("Mensaje del Sistema", "Haz cancelado la operación!", "error");
                                }
                            });
                };

                $scope.listar();
            }]);