'use strict'
app
        .controller('listAccesoFingerCtrl', ['$state', '$scope', 'config', 'AccesoFingerService', '$cookies', '$base64', '$filter', '$rootScope', '$http', function ($state, $scope, config, AccesoFingerService, $cookies, $base64, $filter, $rootScope, $http) {
                $scope.accesos = {};
                $scope.accesoAgregar = {};
                $scope.accesoModificar = {};
                $scope.aModificar = "";
                document.getElementById("fecha").valueAsDate = null;
                $("#paginaActual").val("1");
                $scope.totalItems = 0;
                $scope.currentPage = 0;
                $scope.pageChangedFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };
                $scope.pageChangedSinFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };

                $scope.listar = function () {
                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    AccesoFingerService.fetch(config.cantPaginacion, offSet).success(function (data) {
                        $scope.accesos = data;
                        $scope.paginar();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };
                $scope.limpiar = function () {
                    document.getElementById("fecha").valueAsDate = null;
                    $scope.filterText = "";
                    $scope.edificio = "";
                };

                //PAGINACION

                $scope.paginar = function () {
                    AccesoFingerService.count().success(function (data) {
                        $scope.catidad = data;

                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "none");
                        $("#paginacionSinFiltro").css("display", "inline");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $rootScope.$on("$locationChangeStart", function (event, data) {
                    var pag = parseInt(data.replace("http://" + config.ipFront + ":8017/menu.html#/:pagina=", ""));
                    if (!isNaN(pag)) {
                        $("#paginaActual").val(pag);
                        $scope.buscarSegunPagina();
                    }
                });

                $scope.paginarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.edificio === "" || $scope.edificio === undefined) {
                        $scope.edi = null;
                    } else {
                        $scope.edi = $scope.edificio;
                    }
                    if ($scope.fecha === "" || $scope.fecha === undefined) {
                        $scope.fec = null;
                    } else {
                        var date = new Date($('#fecha').val());
                        var day = date.getDate() + 1;
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();

                        var fecha = [year, ("0" + month).slice(-2), ("0" + day).slice(-2)].join('-');

                        $scope.fec = fecha;
                        if ($scope.fec === "NaN-aN-aN") {
                            $scope.fec = null;
                        }
                    }
                    AccesoFingerService.countFiltro($scope.filt, $scope.fec, $scope.edi).success(function (data) {
                        $scope.catidad = data;
                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "inline");
                        $("#paginacionSinFiltro").css("display", "none");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
//                    $("#paginacionesHistorial").html('<ul class="pagination"><li class="disabled"><a href="#">&laquo;</a></li><li class="active"><a href="#">1</a></li><li ><a href="#">2</a></li><li><a href="#">&raquo;</a></li></ul>');
                };

                $scope.generarReporte = function () {
                    $scope.jsonData = $cookies.getObject("usuarioLogueado");
                    $scope.userLog = $scope.jsonData.usuarioUsuario;

                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.edificio === "" || $scope.edificio === undefined) {
                        $scope.edi = null;
                    } else {
                        $scope.edi = $scope.edificio;
                    }
                    if ($scope.fecha === "" || $scope.fecha === undefined) {
                        $scope.fec = null;
                    } else {
                        var date = new Date($('#fecha').val());
                        var day = date.getDate() + 1;
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();

                        var fecha = [year, ("0" + month).slice(-2), ("0" + day).slice(-2)].join('-');

                        $scope.fec = fecha;
                        if ($scope.fec === "NaN-aN-aN") {
                            $scope.fec = null;
                        }
                    }
                    $http.get(config.backend + '/accesoFinger/generarReporte/' + $scope.filt + '/' + $scope.edi + '/' + $scope.fec + '/' + $scope.userLog, {responseType: 'arraybuffer'})
                            .success(function (data) {
                                var file = new Blob([data], {type: 'application/pdf'});
                                var fileURL = URL.createObjectURL(file);
                                window.open(fileURL);
                            });
                };

                $scope.listarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.edificio === "" || $scope.edificio === undefined) {
                        $scope.edi = null;
                    } else {
                        $scope.edi = $scope.edificio;
                    }
                    if ($scope.fecha === "" || $scope.fecha === undefined) {
                        $scope.fec = null;
                    } else {
                        var date = new Date($('#fecha').val());
                        var day = date.getDate() + 1;
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();

                        var fecha = [year, ("0" + month).slice(-2), ("0" + day).slice(-2)].join('-');

//                        alert(fecha);
//                        console.log("-->> " + fecha);
                        $scope.fec = fecha;
                        if ($scope.fec === "NaN-aN-aN") {
                            $scope.fec = null;
                        }
                    }
                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    AccesoFingerService.fetchFiltro(config.cantPaginacion, offSet, $scope.filt, $scope.fec, $scope.edi).success(function (data) {
                        $scope.accesos = data;
                        $scope.paginarConFiltro();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }

                $scope.buscar = function () {
                    $scope.accesos = [];
                    location.href = "http://" + config.ipFront + ":8017/menu.html#/:pagina=1";
                    if ($scope.filterText !== "" || $scope.fecha !== "" || $scope.edificio !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
//                    }
                };

                $scope.buscarSegunPagina = function () {
                    $scope.accesos = [];
//                    location.href = "http://" + config.ipFront + ":8017/menu.html#/:pagina=1";
                    if ($scope.filterText !== "" || $scope.fecha !== "" || $scope.edificio !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
                };

                //FIN PAGINACION

                $scope.reporte = function () {
                    $scope.jsonData = $cookies.getObject("usuarioLogueado");
                    $scope.userLog = $scope.jsonData.usuarioUsuario;

                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.fecha === "" || $scope.fecha === undefined) {
                        $scope.fec = null;
                    } else {
                        var date = new Date($('#fecha').val());
                        var day = date.getDate() + 1;
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();

                        var fecha = [year, ("0" + month).slice(-2), ("0" + day).slice(-2)].join('-');
                        $scope.fec = fecha;
                        if ($scope.fec === "NaN-aN-aN") {
                            $scope.fec = null;
                        }
                    }
                    if ($scope.edificio === "" || $scope.edificio === undefined) {
                        $scope.edi = null;
                    } else {
                        $scope.edi = $scope.edificio;
                    }

                    $scope.result = {};
                    $http.get(config.backend + '/accesoFinger/reportarPorDescripcion/' + $scope.filt + '/' + $scope.fec + '/' + $scope.edi + '/' + $scope.userLog, {responseType: 'arraybuffer'})
                            .success(function (data) {
                                var file = new Blob([data], {type: 'application/pdf'});
                                var fileURL = URL.createObjectURL(file);
                                window.open(fileURL);
                            });
                };

                $scope.atras = function () {
                    location.reload();
                };

                $scope.$watch('filterText', function () {
                    $("#paginaActual").val("1");
                })
                $scope.$watch('fecha', function () {
                    $("#paginaActual").val("1");
                })
                $scope.$watch('edificio', function () {
                    $("#paginaActual").val("1");
                })
                $scope.buscar();
            }]);
