'use strict'
app
        .factory('RolFuncionService', function ($http, config) {
            var url = config.backend + "/rolesFunciones";
            return {
                listar: function () {
                    return $http.get(url);
                },
                listarPorId: function (id) {
                    return $http.get(url + "/" + id);
                },
                deleteByIdRol: function (id) {
                    return $http.get(url + "/deleteByIdRol/" + id);
                },
                listarPorIdModulo: function (id) {
                    return $http.get(url + "/modulo/" + id);
                },
                listarPorIdRol: function (id) {
                    return $http.get(url + "/listarPorIdRol/" + id);
                },
                crear: function (data) {
                    return $http.post(url, data);
                },
                actualizar: function (data) {
                    return $http.put(url, data);
                },
                eliminar: function (id) {
                    return $http.delete(url + "/" + id);
                }
            };
        });