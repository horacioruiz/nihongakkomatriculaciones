'use strict'
app
        .factory('UsuarioRolService', function ($http, config) {
            var url = config.backend + "/usuariosRoles";
            return {
                listar: function () {
                    return $http.get(url);
                },
                listarPorId: function (id) {
                    return $http.get(url + "/" + id);
                },
                deleteByIdUsuario: function (id) {
                    return $http.get(url + "/deleteByIdUsuario/" + id);
                },
                getByIdUsu: function (id) {
                    return $http.get(url + "/getByIdUsu/" + id);
                },
                generarReporte: function (rol, usuLogin) {
                    return $http.get(url + "/generarReporte/" + rol + "/" + usuLogin);
                },
                listarPorIdUsuario: function (id) {
                    return $http.get(url + "/listarPorIdUsuario/" + id);
                },
                crear: function (data) {
                    return $http.post(url, data);
                },
                add: function (idUsu, idRol) {
                    return $http.get(url + "/add/" + idUsu + "/" + idRol);
                },
                deleteData: function (idUsu) {
                    return $http.get(url + "/delete/" + idUsu);
                },
                actualizar: function (data) {
                    return $http.put(url, data);
                },
                eliminar: function (id) {
                    return $http.delete(url + "/" + id);
                },
                modEliminar: function (data) {
                    return $http.put(url + "/eliminar", data);
                }
            };
        });