'use strict'
app
        .factory('ConsumoService', function ($http, config) {
            var url = config.backend + "/consumo";
            return {
                listar: function () {
                    return $http.get(url);
                },
                listarPorId: function (id) {
                    return $http.get(url + "/" + id);
                },
                actualizarDatos: function (id, fin, potencia) {
                    return $http.get(url + "/actualizar/" + id + "/" + fin + "/" + potencia);
                },
                arduinoIniciar: function (ip) {
                    return $http.get("http://" + ip + "/LED=T");
//                    return $http.get(url + "/arduinoPrueba");
                },
                arduinoGetPotencia: function (ip) {
                    return $http.get("http://" + ip + "/LED=F");
//                    return $http.get(url + "/arduinoGetPotencia");
                },
                fetch: function (limit, offset) {
                    return $http.get(url + "/fetch/" + limit + "/" + offset);
                },
                fetchFiltro: function (limit, offset, tipoEdi, comp, fecIni, edificio) {
                    return $http.get(url + "/fetchFiltro/" + limit + "/" + offset + "/" + tipoEdi + "/" + comp + "/" + fecIni + "/" + edificio);
                },
                count: function () {
                    return $http.get(url + "/count/");
                },
                countFiltro: function (tipoEdi, comp, fecIni, edificio) {
                    return $http.get(url + "/countFiltro/" + tipoEdi + "/" + comp + "/" + fecIni + "/" + edificio);
                },
                reportar: function (nodo, depe, comp, fecIni, usuario, edificio) {
                    return $http.get(url + "/reportar/" + nodo + "/" + depe + "/" + comp + "/" + fecIni + "/" + usuario + "/" + edificio);
                },
                crear: function (data) {
                    return $http.post(url, data);
                },
                actualizar: function (data) {
                    return $http.put(url, data);
                },
                eliminar: function (id) {
                    return $http.delete(url + "/" + id);
                }
            };
        });