'use strict'
app
        .factory('ControlAccesoService', function ($http, config) {
            var url = config.backend + "/controles";
            var url2 = config.backend + "/controlNodo";
            return {
                listar: function () {
                    return $http.get(url);
                },
                listarPorId: function (id) {
                    return $http.get(url + "/" + id);
                },
                count: function (count) {
                    return $http.get(url + "/count");
                },
                fetch: function (limit, offset) {
                    return $http.get(url + "/fetch/" + limit + "/" + offset);
                },
                countFiltro: function (usu, fecha, nom, ape) {
                    return $http.get(url + "/countFiltro/" + usu + "/" + fecha + "/" + nom + "/" + ape);
                },
                fetchFiltro: function (limit, offset, usu, fecha, nom, ape) {
                    return $http.get(url + "/fetchFiltro/" + limit + "/" + offset + "/" + usu + "/" + fecha + "/" + nom + "/" + ape);
                },
                listarPorControl: function (id) {
                    return $http.get(url2 + "/listarPorControl/" + id);
                },
                getMaxFingerPrint: function () {
                    return $http.get(url + "/getMaxFingerPrint");
                },
                getLastInsert: function () {
                    return $http.get(url + "/getLastInsert");
                },
                getCodeDistinct: function (code) {
                    return $http.get(url + "/getCodeDistinct/" + code);
                },
                getFingerPrintByCode: function (code) {
                    return $http.get(url + "/getFingerPrintByCode/" + code);
                },
                getFingerPrinbyStatus: function () {
                    return $http.get(url + "/getFingerPrinbyStatus");
                },
                getAllCountByTrue: function () {
                    return $http.get(url + "/getAllCountByTrue");
                },
                getFetchAllByTrue: function (limite, inicio, nombre, apellido) {
                    return $http.get(url + "/getFetchAllByTrue/" + limite + "/" + inicio + "/" + nombre + "/" + apellido);
                },
                actualizarEstado: function (id) {
                    return $http.put(url + "/baja/" + id);
                },
                crear: function (data) {
                    return $http.post(url, data);
                },
                actualizar: function (data) {
                    return $http.put(url, data);
                },
                eliminar: function (id) {
                    return $http.delete(url + "/" + id);
                }
            };
        });