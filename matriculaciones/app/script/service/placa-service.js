'use strict'
app
        .factory('PlacaService', function ($http, config) {
            var url = config.backend + "/placa";
            return {
                listar: function () {
                    return $http.get(url);
                },
                listarTipoPlaca: function () {
                    return $http.get(url + "/tipoPlaca");
                },
                listarPorId: function (id) {
                    return $http.get(url + "/" + id);
                },
                listarPlacaNodoTipoCompo: function (id) {
                    return $http.get(url + "/placaNodoTipoCompo/" + id);
                },
                tipoPlacaPin: function (id) {
                    return $http.get(url + "/tipoPlacaPin/" + id);
                },
                crear: function (data) {
                    return $http.post(url, data);
                },
                actualizar: function (data) {
                    return $http.put(url, data);
                },
                eliminar: function (id) {
                    return $http.delete(url + "/" + id);
                },
                modEliminar: function (data) {
                    return $http.put(url + "/eliminar", data);
                },
                fetch: function (limit, offset) {
                    return $http.get(url + "/fetch/" + limit + "/" + offset);
                },
                fetchFiltro: function (limit, offset, descri, ip, tipo) {
                    return $http.get(url + "/fetchFiltro/" + limit + "/" + offset + "/" + descri + "/" + ip + "/" + tipo);
                },
                count: function () {
                    return $http.get(url + "/count/");
                },
                countFiltro: function (descri, ip, tipo) {
                    return $http.get(url + "/countFiltro/" + descri + "/" + ip + "/" + tipo);
                }
            };
        });