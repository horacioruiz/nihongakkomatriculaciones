'use strict'
app
        .controller('listPlacasCtrl', ['$state', '$scope', 'config', 'PlacaService', '$cookies', '$base64', 'TipoPlacaService', '$http', '$rootScope', function ($state, $scope, config, PlacaService, $cookies, $base64, TipoPlacaService, $http, $rootScope) {
//                console.log("HOLAAA");

                $scope.placas = {};
                $scope.placaAgregar = {};
                $scope.placaModificar = {};
                $scope.placaModificar2 = {};
                $scope.ipAddress1 = "";
                $scope.idTipoPlaca = 0;
                $scope.ipModificar = "";

                $("#paginaActual").val("1");
                $scope.totalItems = 0;
                $scope.currentPage = 0;
                $scope.pageChangedFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };
                $scope.pageChangedSinFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };


                $scope.placaAgregar.tipoPlaca = {};
                var myMap = new Map();
                var myMapPlaca = new Map();

                $scope.limpiar = function () {
                    $scope.filterText = "";
                    $scope.tipo = "";
                    $scope.ipAddress1 = "";
                };

                $scope.listar = function () {
                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    PlacaService.fetch(config.cantPaginacion, offSet).success(function (data) {
                        $scope.placas = data;
                        angular.forEach(data, function (value, key) {
                            myMapPlaca.set(value.ipPlaca, value.ipPlaca);
                        });
                        $scope.paginar();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                //PAGINACION
                $scope.paginar = function () {
                    PlacaService.count().success(function (data) {
                        $scope.catidad = data;
                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "none");
                        $("#paginacionSinFiltro").css("display", "inline");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $rootScope.$on("$locationChangeStart", function (event, data) {
                    var pag = parseInt(data.replace("http://" + config.ipFront + ":8017/menu.html#/:pagina=", ""));
                    if (!isNaN(pag)) {
                        $("#paginaActual").val(pag);
                        $scope.buscarSegunPagina();
                    }
                });

                $scope.paginarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.tipo === "" || $scope.tipo === undefined) {
                        $scope.tip = null;
                    } else {
                        $scope.tip = $scope.tipo;
                    }
                    if ($scope.ipAddress1 === "" || $scope.ipAddress1 === undefined) {
                        $scope.ips = null;
                    } else {
                        $scope.ips = $scope.ipAddress1;
                    }
                    PlacaService.countFiltro($scope.filt, $scope.ips, $scope.tip).success(function (data) {
                        $scope.catidad = data;

                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "inline");
                        $("#paginacionSinFiltro").css("display", "none");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
//                    $("#paginacionesPlaca").html('<ul class="pagination"><li class="disabled"><a href="#">&laquo;</a></li><li class="active"><a href="#">1</a></li><li ><a href="#">2</a></li><li><a href="#">&raquo;</a></li></ul>');
                };

                $scope.generarReporte = function () {
                    $scope.jsonData = $cookies.getObject("usuarioLogueado");
                    $scope.userLog = $scope.jsonData.usuarioUsuario;

                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.tipo === "" || $scope.tipo === undefined) {
                        $scope.tip = null;
                    } else {
                        $scope.tip = $scope.tipo;
                    }
                    if ($scope.ipAddress1 === "" || $scope.ipAddress1 === undefined) {
                        $scope.ips = null;
                    } else {
                        $scope.ips = $scope.ipAddress1;
                    }

                    $http.get(config.backend + '/placa/generarReporte/' + $scope.filt + '/' + $scope.ips + '/' + $scope.tip + '/' + $scope.userLog, {responseType: 'arraybuffer'})
                            .success(function (data) {
                                var file = new Blob([data], {type: 'application/pdf'});
                                var fileURL = URL.createObjectURL(file);
                                window.open(fileURL);
                            });
                }

                $scope.listarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.tipo === "" || $scope.tipo === undefined) {
                        $scope.tip = null;
                    } else {
                        $scope.tip = $scope.tipo;
                    }
                    if ($scope.ipAddress1 === "" || $scope.ipAddress1 === undefined) {
                        $scope.ips = null;
                    } else {
                        $scope.ips = $scope.ipAddress1;
                    }
                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    PlacaService.fetchFiltro(config.cantPaginacion, offSet, $scope.filt, $scope.ips, $scope.tip).success(function (data) {
                        $scope.placas = data;
                        $scope.paginarConFiltro();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }

                $scope.buscar = function () {
//                    $scope.valor = 0;
//                    if ($scope.filterText === "" || $scope.filterText === undefined) {
//                        $scope.valor++;
//                    }
//                    if ($scope.dependencia === "" || $scope.dependencia === undefined) {
//                        $scope.valor++;
//                    }
//                    if ($scope.componente === "" || $scope.componente === undefined) {
//                        $scope.valor++;
//                    }
//                    if ($scope.valor < 2) {
////                        alert($scope.valor);
//                        swal("Mensaje del Sistema!", "El filtro es por un solo campo", "error");
//                    } else {
                    $scope.placas = [];
                    if ($scope.filterText !== "" || $scope.ipAddress1 !== "" || $scope.tipo !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
//                    }
                };

                $scope.buscarSegunPagina = function () {
                    $scope.placas = [];
//                    location.href = "http://" + config.ipFront + ":8017/menu.html#/:pagina=1";
                    if ($scope.filterText !== "" || $scope.ipAddress1 !== "" || $scope.tipo !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
                };

                //FIN DE PAGINACION

                $scope.registrar = function () {
                    $scope.placaAgregar.tipoPlaca = {};
                    if ($scope.placaAgregar.descriPlaca === "" || $scope.placaAgregar.descriPlaca === undefined || $scope.placaAgregar.descriPlaca === null ||
                            $scope.ipAddress1 === "" || $scope.ipAddress1 === undefined || $scope.ipAddress1 === null) {
                        swal("Mensaje del Sistema!", "Los campos no deben quedar vacío", "error");
                    } else {
                        if (myMapPlaca.has($scope.ipAddress1)) {
                            swal("Mensaje del Sistema!", "La ip ingresada ya esta siendo utilizada", "error");
                        } else {
                            if ($("#valid").html() === undefined) {
                                swal("Mensaje del Sistema!", "Verificar que los Datos de IP sean los correctos", "error");
                            } else {
                                $scope.placaAgregar.tipoPlaca.idTipoPlaca = myMap.get($scope.idTipoPlaca);
                                $scope.placaAgregar.ipPlaca = $scope.ipAddress1;
                                $scope.jsonUsu = $cookies.getObject("usuarioLogueado");
                                $scope.placaAgregar.usuAltaPlaca = $scope.jsonUsu.usuarioUsuario;
                                $scope.placaAgregar.usuModPlaca = $scope.jsonUsu.usuarioUsuario;
                                PlacaService.crear($scope.placaAgregar).success(function (data) {
                                    if (data === true) {
                                        swal("Mensaje del Sistema!", "Datos Agregados exitosamente", "success");
                                        $('#modalAgregar').modal('hide');
                                        $scope.ipAddress1 = "";
                                        $scope.listar();
                                    } else {
                                        swal("Mensaje del Sistema!", "Datos no Agregados, verique que los campos no estén vacío", "error");
                                    }
                                }).error(function (e) {
                                    swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                                });
                            }
                        }
                    }
                };

                $scope.atras = function () {
                    location.reload();
                };

                $scope.actualizar = function () {
                    if ($scope.placaModificar.descriPlaca === "" || $scope.placaModificar.descriPlaca === undefined || $scope.placaModificar.descriPlaca === null ||
                            $scope.ipAddress1 === "" || $scope.ipAddress1 === undefined || $scope.ipAddress1 === null) {
                        swal("Mensaje del Sistema!", "Los campos no deben quedar vacío", "error");
                    } else {
                        if (myMapPlaca.has($scope.ipAddress1)) {
                            swal("Mensaje del Sistema!", "La ip ingresada ya esta siendo utilizada", "error");
                        } else {
                            if ($("#valid2").html() === undefined) {
                                if ($scope.ipModificar === $scope.ipAddress1) {
                                    $scope.ipModificar = "";
                                    $scope.placaModificar.ipPlaca = $scope.ipAddress1;
                                    $scope.jsonUsu = $cookies.getObject("usuarioLogueado");
                                    $scope.placaModificar.usuModPlaca = $scope.jsonUsu.usuarioUsuario;
                                    PlacaService.actualizar($scope.placaModificar).success(function (data) {
                                        if (data === true) {
                                            swal("Mensaje del Sistema!", "Datos Modificados exitosamente", "success");
                                            $('#modalModificar').modal('hide');
                                            $scope.ipAddress1 = "";
                                            $scope.listar();
                                        } else {
                                            swal("Mensaje del Sistema!", "Datos no Modificados", "error");
                                        }
                                    }).error(function (e) {
                                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                                    });
                                } else {
                                    swal("Mensaje del Sistema!", "Verificar que los Datos de IP sean los correctos", "error");
                                }
                            } else {
                                $scope.placaModificar.ipPlaca = $scope.ipAddress1;
                                $scope.jsonUsu = $cookies.getObject("usuarioLogueado");
                                $scope.placaModificar.usuModPlaca = $scope.jsonUsu.usuarioUsuario;
                                PlacaService.actualizar($scope.placaModificar).success(function (data) {
                                    if (data === true) {
                                        swal("Mensaje del Sistema!", "Datos Modificados exitosamente", "success");
                                        $('#modalModificar').modal('hide');
                                        $scope.ipAddress1 = "";
                                        $scope.listar();
                                    } else {
                                        swal("Mensaje del Sistema!", "Datos no Modificados", "error");
                                    }
                                }).error(function (e) {
                                    swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                                });
                            }
                        }
                    }
                };

                $scope.modificar = function (placas) {
                    $scope.ipModificar = "";
                    $scope.placaModificar.tipoPlaca = {};
                    $scope.placaModificar.descriPlaca = placas.descriPlaca;
                    $scope.ipAddress1 = placas.ipPlaca;
                    $scope.ipModificar = placas.ipPlaca;
                    $scope.placaModificar.idPlaca = placas.idPlaca;
                    $scope.placaModificar.fechaAltaPlaca = placas.fechaAltaPlaca;
                    $scope.placaModificar.fechaModPlaca = placas.fechaModPlaca;
                    $scope.placaModificar.usuAltaPlaca = placas.usuAltaPlaca;
                    $scope.placaModificar.usuModPlaca = placas.usuModPlaca;
                    $scope.placaModificar.estadoPlaca = placas.estadoPlaca;
                    $scope.placaModificar.estadoPlaca = placas.estadoPlaca;
                    $scope.placaModificar.tipoPlaca.idTipoPlaca = placas.tipoPlaca.idTipoPlaca;
                    $("#modalModificar").modal("show");
                };

                $scope.cargarCombo = function () {
                    //PERSONA FILTRO
                    $scope.nestedItemsLevel1 = [];

                    TipoPlacaService.listar().success(function (d) {
                        angular.forEach(d, function (value, key) {
                            $scope.nestedItemsLevel1.push(value.descriTipo);
                            myMap.set(value.descriTipo, value.idTipoPlaca);
                        });
                    });
                };

                $scope.agregar = function () {
                    $scope.placaAgregar = {};
                    $scope.idTipoPlaca = "";
                    $scope.ipAddress1 = "";
                    $scope.cargarCombo();
                };

                $scope.eliminar = function (marc) {
                    $scope.datoEliminar = {};
                    $scope.datoEliminar.idPlaca = marc.idPlaca;
                    $scope.jsonUsu = $cookies.getObject("usuarioLogueado");
                    $scope.datoEliminar.usuModPlaca = $scope.jsonUsu.usuarioUsuario;
                    swal({
                        title: "Mensaje del Sistema",
                        text: "Seguro que quiere eliminar " + marc.descriPlaca + "?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Si",
                        cancelButtonText: "No",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                            function (isConfirm) {
                                if (isConfirm) {
                                    PlacaService.eliminar(marc.idPlaca).success(function (data) {
//                                    PlacaService.modEliminar($scope.datoEliminar).success(function (data) {
                                        if (data === true) {
                                            swal("Mensaje del Sistema!", "Datos Eliminados exitosamente", "success");
                                            $scope.placaModificar = {};
                                            $scope.listar();
                                        } else {
                                            swal("Mensaje del Sistema!", "No se pudo eliminar la Placa, verifique Dependencias", "error");
                                        }
                                    }).error(function (e) {
                                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                                    });
                                } else {
                                    swal("Mensaje del Sistema", "Haz cancelado la operación!", "error");
                                }
                            });
                };
                $scope.$watch('filterText', function () {
                    $("#paginaActual").val("1");
                })
                $scope.$watch('ip', function () {
                    $("#paginaActual").val("1");
                })
                $scope.$watch('tipo', function () {
                    $("#paginaActual").val("1");
                })
                $scope.buscar();
            }]);