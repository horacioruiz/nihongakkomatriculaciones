'use strict'
app
        .controller('listPlacaPinCtrl', ['$state', '$scope', 'config', 'PlacaPinService', '$cookies', '$base64', 'PlacaService', 'TipoPlacaPinService', '$http', '$rootScope', function ($state, $scope, config, PlacaPinService, $cookies, $base64, PlacaService, TipoPlacaPinService, $http, $rootScope) {

                $scope.placaPin = {};
                $scope.placaPinAgregar = {};
                $scope.placaPinModificar = {};
                $scope.placaPinAgregar.nodo = {};
                $scope.placaPinAgregar.placaPin = {};
                $("#paginaActual").val("1");
                $scope.totalItems = 0;
                $scope.currentPage = 0;
                $scope.pageChangedFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };
                $scope.pageChangedSinFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };

                $scope.aModificar = "";

                var myMapPlaca = new Map();
                var myMapPin = new Map();

                $scope.limpiar = function () {
                    $scope.filterText = "";
                    $scope.tipo = "";
                };

                $scope.listar = function () {
                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    PlacaPinService.fetch(config.cantPaginacion, offSet).success(function (data) {
                        $scope.placaPin = data;
                        $scope.paginar();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $scope.paginar = function () {
                    PlacaPinService.count().success(function (data) {
                        $scope.catidad = data;

                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "none");
                        $("#paginacionSinFiltro").css("display", "inline");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $rootScope.$on("$locationChangeStart", function (event, data) {
                    var pag = parseInt(data.replace("http://" + config.ipFront + ":8017/menu.html#/:pagina=", ""));
                    if (!isNaN(pag)) {
                        $("#paginaActual").val(pag);
                        $scope.buscarSegunPagina();
                    }
                });

                $scope.paginarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.tipo === "" || $scope.tipo === undefined) {
                        $scope.tip = null;
                    } else {
                        $scope.tip = $scope.tipo;
                    }
                    PlacaPinService.countFiltro($scope.filt, $scope.tip).success(function (data) {
                        $scope.catidad = data;

                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "inline");
                        $("#paginacionSinFiltro").css("display", "none");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
//                    $("#paginacionesPlacaPin").html('<ul class="pagination"><li class="disabled"><a href="#">&laquo;</a></li><li class="active"><a href="#">1</a></li><li ><a href="#">2</a></li><li><a href="#">&raquo;</a></li></ul>');
                };

                $scope.listarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.tipo === "" || $scope.tipo === undefined) {
                        $scope.tip = null;
                    } else {
                        $scope.tip = $scope.tipo;
                    }
                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    PlacaPinService.fetchFiltro(config.cantPaginacion, offSet, $scope.filt, $scope.tip).success(function (data) {
                        $scope.placaPin = data;
                        $scope.paginarConFiltro();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $scope.buscar = function () {
                    $scope.placaPin = [];
                    location.href = "http://" + config.ipFront + ":8017/menu.html#/:pagina=1";
                    if ($scope.filterText !== "" || $scope.tipo !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
//                    }
                };

                $scope.buscarSegunPagina = function () {
                    $scope.placaPin = [];
                    if ($scope.filterText !== "" || $scope.tipo !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
                };

                //FIN DE PAGINACION



                $scope.onChanged = function () {
                    $scope.nestedItemsLevelPlacaPin = [];
                    $scope.idPlacaPin = "";
                    PlacaService.listarPorId(myMapPlaca.get($scope.idPlaca)).success(function (data) {
//                        $scope.placaPinAgregar.descriEdificio = data.edificio.descriEdificio;
//                        $scope.placaPinAgregar.descriPlaca = data.placa.descriPlaca;
                        $scope.placaPinAgregar.descriTipo = data.tipoPlaca.descriTipo;
//                        $scope.placaPinAgregar.descriDependencia = data.dependencia.descriDependencia;
//                        $scope.placaPinAgregar.descriComponente = data.componente.descriComponente;
                        TipoPlacaPinService.listarPorPlaca(data.tipoPlaca.idTipoPlaca).success(function (d) {
                            angular.forEach(d, function (value, key) {
                                $scope.nestedItemsLevelPlacaPin.push(value.pin);
                                myMapPin.set(value.pin, value.idTipoPlacaPin);
                            });
                        });
                    });
                };

                $scope.agregar = function () {
                    $scope.placaPinAgregar.placa = {};
//                    $scope.placaPinAgregar.descriEdificio = "";
//                    $scope.placaPinAgregar.descriPlaca = "";
//                    $scope.placaPinAgregar.descriDependencia = "";
//                    $scope.placaPinAgregar.descriComponente = "";
                    $scope.placaPinAgregar.tipoPlacaPin = {};

                    $scope.placaPinAgregar.descriTipo = "";
                    myMapPlaca = new Map();
                    myMapPin = new Map();
                    $scope.idPlaca = "";
                    $scope.idTipoPlacaPin = "";
                    //PERSONA FILTRO
                    $scope.nestedItemsLevel1 = [];
//                    $scope.nestedItemsLevelPlacaPin = [];
                    PlacaService.listar().success(function (d) {
                        angular.forEach(d, function (value, key) {
                            $scope.nestedItemsLevel1.push(value.descriPlaca);
                            myMapPlaca.set(value.descriPlaca, value.idPlaca);
                        });
                    });
                };
                $scope.registrar = function () {
                    $scope.placaPinAgregar.placa.idPlaca = myMapPlaca.get($scope.idPlaca);
                    $scope.placaPinAgregar.tipoPlacaPin.idTipoPlacaPin = myMapPin.get($scope.idTipoPlacaPin);

                    PlacaPinService.crear($scope.placaPinAgregar).success(function (data) {
                        if (data === true) {
                            swal("Mensaje del Sistema!", "Datos Agregados exitosamente", "success");
                            $('#modalAgregar').modal('hide');
//                            $scope.placaPinAgregar = {};
                            $scope.listar();
                        } else {
                            swal("Mensaje del Sistema!", "Datos no Agregados, verique que los campos no estén vacíos", "error");
                        }
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };
                $scope.atras = function () {
                    location.reload();
                };

                $scope.reporte = function () {
                    $scope.jsonData = $cookies.getObject("usuarioLogueado");
                    $scope.userLog = $scope.jsonData.usuarioUsuario;

                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.tipo === "" || $scope.tipo === undefined) {
                        $scope.tip = null;
                    } else {
                        $scope.tip = $scope.tipo;
                    }

                    $scope.result = {};
                    $http.get(config.backend + '/placaPin/reportarPorDescripcion/' + $scope.filt + '/' + $scope.tip + '/' + $scope.userLog, {responseType: 'arraybuffer'})
                            .success(function (data) {
                                var file = new Blob([data], {type: 'application/pdf'});
                                var fileURL = URL.createObjectURL(file);
                                window.open(fileURL);
                            });
//                    }
                };
//                $scope.eliminar = function (placaPin) {
//                    var r = confirm("Seguro que quiere eliminar el Nodo " + placaPin.nodo.descriPlaca + "?");
//                    if (r === true) {
//                        PlacaPinService.eliminar(placaPin.idPlacaPin).success(function (data) {
//                            if (data === true) {
//                                swal('Eliminado!',
//                                        'Haz eliminado exitosamente.',
//                                        'success'
//                                        );
//                                $scope.listar();
//                            }
//                        });
//                    } else {
//                        swal('Cancelado',
//                                'Haz cancelado la eliminación',
//                                'error'
//                                );
//                    }
//                };

                $scope.eliminar = function (placaPin) {
                    swal({
                        title: "Mensaje del Sistema",
                        text: "Seguro que quiere eliminar La placa " + placaPin.placa.descriPlaca + ", con el Pin " + placaPin.tipoPlacaPin.pin + "?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Si",
                        cancelButtonText: "No",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                            function (isConfirm) {
                                if (isConfirm) {
                                    PlacaPinService.eliminar(placaPin.idPlacaPin).success(function (data) {
                                        if (data === true) {
                                            swal('Eliminado!',
                                                    'Haz eliminado exitosamente.',
                                                    'success'
                                                    );
                                            $scope.listar();
                                        }
                                    }).error(function (e) {
                                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                                    });
                                } else {
                                    swal("Mensaje del Sistema", "Haz cancelado la operación!", "error");
                                }
                            });
                };

                $scope.$watch('filterText', function () {
                    $("#paginaActual").val("1");
                })
                $scope.$watch('tipo', function () {
                    $("#paginaActual").val("1");
                })
                $scope.buscar();
            }]);