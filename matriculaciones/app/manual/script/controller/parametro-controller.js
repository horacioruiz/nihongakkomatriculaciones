'use strict'
app
        .controller('listParametrosCtrl', ['$state', '$scope', '$http', 'config', 'ParametroService', '$cookies', '$base64', function ($state, $scope, $http, config, ParametroService, $cookies, $base64) {

                $scope.parametros = {};
                $scope.parametroAgregar = {};
                $scope.parametroModificar = {};

                $("#paginaActual").val("1");
                $scope.totalItems = 0;
                $scope.currentPage = 0;
                $scope.pageChangedFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };
                $scope.pageChangedSinFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };

//                FIN DE PAGINACION

                $scope.listar = function () {
                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    ParametroService.fetch(config.cantPaginacion, offSet).success(function (data) {
                        $scope.parametros = data;
                        $scope.paginar();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };
                $scope.paginar = function () {
                    ParametroService.count().success(function (data) {
                        $scope.catidad = data;
                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "none");
                        $("#paginacionSinFiltro").css("display", "inline");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $scope.paginarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    ParametroService.countFiltro($scope.filt).success(function (data) {
                        $scope.catidad = data;

                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "inline");
                        $("#paginacionSinFiltro").css("display", "none");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };
                $scope.listarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }

                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    ParametroService.fetchFiltro(config.cantPaginacion, offSet, $scope.filt).success(function (data) {
                        $scope.parametros = data;
                        $scope.paginarConFiltro();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }

                $scope.generarReporte = function () {
                    $scope.jsonData = $cookies.getObject("usuarioLogueado");
                    $scope.userLog = $scope.jsonData.usuarioUsuario;

                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    $http.get(config.backend + '/parametro/generarReporte/' + $scope.filt + '/' + $scope.userLog, {responseType: 'arraybuffer'})
                            .success(function (data) {
                                var file = new Blob([data], {type: 'application/pdf'});
                                var fileURL = URL.createObjectURL(file);
                                window.open(fileURL);
                            });
                };

                $scope.buscar = function () {
                    $scope.parametros = [];
                    if ($scope.filterText !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
//                    }
                };
                $scope.buscarSegunPagina = function () {
                    $scope.parametros = [];
                    if ($scope.filterText !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
                };

                $scope.registrar = function () {
                    $scope.jsonUsu = $cookies.getObject("usuarioLogueado");
                    $scope.parametroAgregar.usuAltaParametro = $scope.jsonUsu.usuarioUsuario;
                    $scope.parametroAgregar.usuModParametro = $scope.jsonUsu.usuarioUsuario;

                    if ($("#estadoNodAgregar").prop('checked')) {
                        $scope.parametroAgregar.encendido = true;
                    } else {
                        $scope.parametroAgregar.encendido = false;
                    }

                    if ($scope.parametroAgregar.descriParametro === "" || $scope.parametroAgregar.descriParametro === undefined || $scope.parametroAgregar.descriParametro === null) {
                        swal("Mensaje del Sistema!", "Descripción no debe quedar vacío", "error");
                    } else {
                        ParametroService.crear($scope.parametroAgregar).success(function (data) {
                            if (data === true) {
                                swal("Mensaje del Sistema!", "Datos Agregados exitosamente", "success");
                                $('#modalAgregar').modal('hide');
                                $scope.listar();
                            } else {
                                swal("Mensaje del Sistema!", "Datos no Agregados, verifique que los campos no estén vacíos", "error");
                            }
                        }).error(function (e) {
                            swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                        });
                    }
                };

                $scope.atras = function () {
                    location.reload();
                };

                $scope.agregar = function () {
                    $scope.parametroAgregar.descriParametro = "";
                    $("#estadoNodAgregar").prop('checked', false);
                };

                $scope.actualizar = function () {
                    if ($scope.parametroAgregar.descriParametro === "" || $scope.parametroAgregar.descriParametro === undefined || $scope.parametroAgregar.descriParametro === null) {
                        swal("Mensaje del Sistema!", "Descripción no debe quedar vacío", "error");
                    } else {
                        ParametroService.actualizar($scope.parametroModificar).success(function (data) {
                            if (data === true) {
                                swal("Mensaje del Sistema!", "Datos Modificados exitosamente", "success");
                                $('#modalModificar').modal('hide');
                                $scope.listar();
                            } else {
                                swal("Mensaje del Sistema!", "Datos no Modificados, verifique que los campos no estén vacíos", "error");
                            }
                        }).error(function (e) {
                            swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                        });
                    }
                };

                $scope.modificar = function (parametros) {
                    $scope.parametroModificar = parametros;
                    if (parametros.encendido === true) {
//                    if (nodos.estadoComponente === true) { --> ORIGINAL
                        $("#estadoNodMod").prop("checked", true);
                        $scope.parametroModificar.encendido = true;
//                        $scope.nodoModificar.estadoComponente = true; --> ORIGINAL
                    } else {
                        $("#estadoNodMod").prop("checked", false);
//                        $scope.nodoModificar.estadoComponente = false; --> ORIGINAL
                        $scope.parametroModificar.encendido = false;
                    }
                    $("#modalModificar").modal("show");
                };

                $scope.eliminar = function (parametros) {
                    swal({
                        title: "Mensaje del Sistema",
                        text: "Seguro que quiere eliminar el Parametro " + parametros.descriParametro + "?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Si",
                        cancelButtonText: "No",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                            function (isConfirm) {
                                if (isConfirm) {
                                    ParametroService.eliminar(parametros.idParametro).success(function (data) {
                                        if (data === true) {
                                            swal('Eliminado!',
                                                    'Haz eliminado exitosamente.',
                                                    'success'
                                                    );
                                            $scope.listar();
                                        } else {
                                            ParametroService.deleteUpdate(parametros.idParametro).success(function (data) {
                                                if (data === true) {
                                                    swal('Eliminado!',
                                                            'Haz eliminado exitosamente.',
                                                            'success'
                                                            );
                                                    $scope.listar();
                                                }
                                            });
                                        }
                                    }).error(function (e) {
                                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                                    });
                                } else {
                                    swal("Mensaje del Sistema", "Haz cancelado la operación!", "error");
                                }
                            });
                };
                $scope.listar();
            }]);