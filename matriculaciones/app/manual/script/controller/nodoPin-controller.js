'use strict'
app
        .controller('listNodoPinCtrl', ['$state', '$scope', 'config', 'NodoPinService', '$cookies', '$base64', 'NodoService', 'TipoPlacaPinService', '$http', '$rootScope', 'PlacaService', function ($state, $scope, config, NodoPinService, $cookies, $base64, NodoService, TipoPlacaPinService, $http, $rootScope, PlacaService) {

                $scope.nodoPin = {};
                $scope.nodoPinAgregar = {};
                $scope.nodoPinModificar = {};
                $scope.nodoPinAgregar.nodo = {};
                $scope.nodoPinAgregar.tipoPlacaPin = {};
                $("#paginaActual").val("1");
                $scope.totalItems = 0;
                $scope.currentPage = 0;
                $scope.pageChangedFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };
                $scope.pageChangedSinFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };
                $scope.descripcionTipoComp = "";

                $scope.aModificar = "";

                var myMapNodo = new Map();
                var myMapPin = new Map();

                $scope.limpiar = function () {
                    $scope.filterText = "";
                    $scope.dependencia = "";
                    $scope.placa = "";
                    $scope.edificio = "";
                };

                $scope.listar = function () {
                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    NodoService.fetchNodoPin(config.cantPaginacion, offSet).success(function (data) {
                        $scope.nodos = data;
                        $scope.paginar();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $scope.paginar = function () {
                    NodoService.countNodoPin().success(function (data) {
                        $scope.catidad = data;
                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "none");
                        $("#paginacionSinFiltro").css("display", "inline");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $rootScope.$on("$locationChangeStart", function (event, data) {
                    var pag = parseInt(data.replace("http://" + config.ipFront + ":8017/menu.html#/:pagina=", ""));
                    if (!isNaN(pag)) {
                        $("#paginaActual").val(pag);
                        $scope.buscarSegunPagina();
                    }
                });

//                $scope.paginarConFiltro = function () {
//                    if ($scope.filterText === undefined || $scope.filterText === "") {
//                        $scope.filt = null;
//                    } else {
//                        $scope.filt = $scope.filterText;
//                    }
//                    if ($scope.dependencia === "" || $scope.dependencia === undefined) {
//                        $scope.dep = null;
//                    } else {
//                        $scope.dep = $scope.dependencia;
//                    }
//                    if ($scope.placa === "" || $scope.placa === undefined) {
//                        $scope.comp = null;
//                    } else {
//                        $scope.comp = $scope.placa;
//                    }
//                    NodoPinService.countFiltro($scope.filt, $scope.dep, $scope.comp).success(function (data) {
//                        $scope.catidad = data;
//                        var cantidad = data / config.cantPaginacion;
//                        var paginacion = cantidad;
//                        var enteroPag = parseInt(cantidad);
//                        if (cantidad > enteroPag) {
//                            paginacion = parseInt(cantidad) + 1;
//                        }
//
//                        var htmlPaginacion = '<ul class="pagination">';//<li class="disabled"><a >&laquo;</a></li>';
//                        for (var i = 0; i < paginacion; i++) {
//                            if (i === 0) {
//                                htmlPaginacion += ' <li id="pag' + (i + 1) + '" class="active"><a href="#:pagina=' + (i + 1) + '">' + (i + 1) + '</a></li>';
//                            } else {
//                                htmlPaginacion += '<li id="pag' + (i + 1) + '" ><a href="#:pagina=' + (i + 1) + '">' + (i + 1) + '</a></li>';
//                            }
//
//                        }
//                        $("#paginacionesNodoPin").html(htmlPaginacion);
//
//                        for (var i = 0; i < paginacion; i++) {
//                            $("#pag" + (i + 1)).removeClass("active");
//                        }
//                        $("#pag" + $("#paginaActual").val()).addClass("active");
//
//                        $("#paginacionesNodoPin").fadeIn("slow");
//                    }).error(function (e) {
//                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
//                    });
//                };

                $scope.paginarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.placa === "" || $scope.placa === undefined) {
                        $scope.pla = null;
                    } else {
                        $scope.pla = $scope.placa;
                    }
                    if ($scope.dependencia === "" || $scope.dependencia === undefined) {
                        $scope.dep = null;
                    } else {
                        $scope.dep = $scope.dependencia;
                    }
                    if ($scope.edificio === "" || $scope.edificio === undefined) {
                        $scope.edi = null;
                    } else {
                        $scope.edi = $scope.edificio;
                    }

                    NodoService.countFiltroNodoPin($scope.filt, null, $scope.dep, $scope.pla, $scope.edi).success(function (data) {
                        $scope.catidad = data;

                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "inline");
                        $("#paginacionSinFiltro").css("display", "none");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $scope.generarReporte = function () {
                    $scope.jsonData = $cookies.getObject("usuarioLogueado");
                    $scope.userLog = $scope.jsonData.usuarioUsuario;

                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.placa === "" || $scope.placa === undefined) {
                        $scope.pla = null;
                    } else {
                        $scope.pla = $scope.placa;
                    }
                    if ($scope.dependencia === "" || $scope.dependencia === undefined) {
                        $scope.dep = null;
                    } else {
                        $scope.dep = $scope.dependencia;
                    }
                    if ($scope.edificio === "" || $scope.edificio === undefined) {
                        $scope.edi = null;
                    } else {
                        $scope.edi = $scope.edificio;
                    }
                    $http.get(config.backend + '/nodoPin/generarReporte/' + $scope.filt + '/' + $scope.pla + '/' + $scope.dep + '/' + $scope.edi + '/' + $scope.userLog, {responseType: 'arraybuffer'})
                            .success(function (data) {
                                var file = new Blob([data], {type: 'application/pdf'});
                                var fileURL = URL.createObjectURL(file);
                                window.open(fileURL);
                            });
                };

//                $scope.listarConFiltro = function () {
//                    if ($scope.filterText === undefined || $scope.filterText === "") {
//                        $scope.filt = null;
//                    } else {
//                        $scope.filt = $scope.filterText;
//                    }
//                    if ($scope.dependencia === "" || $scope.dependencia === undefined) {
//                        $scope.dep = null;
//                    } else {
//                        $scope.dep = $scope.dependencia;
//                    }
//                    if ($scope.placa === "" || $scope.placa === undefined) {
//                        $scope.comp = null;
//                    } else {
//                        $scope.comp = $scope.placa;
//                    }
//                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
//                    NodoPinService.fetchFiltro(config.cantPaginacion, offSet, $scope.filt, $scope.dep, $scope.comp).success(function (data) {
//                        $scope.nodoPin = data;
//                        $scope.paginarConFiltro();
//                    }).error(function (e) {
//                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
//                    });
//                };

                $scope.listarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.placa === "" || $scope.placa === undefined) {
                        $scope.pla = null;
                    } else {
                        $scope.pla = $scope.placa;
                    }
                    if ($scope.dependencia === "" || $scope.dependencia === undefined) {
                        $scope.dep = null;
                    } else {
                        $scope.dep = $scope.dependencia;
                    }
                    if ($scope.edificio === "" || $scope.edificio === undefined) {
                        $scope.edi = null;
                    } else {
                        $scope.edi = $scope.edificio;
                    }
                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    NodoService.fetchFiltroNodoPin(config.cantPaginacion, offSet, $scope.filt, null, $scope.dep, $scope.pla, $scope.edi).success(function (data) {
                        $scope.nodos = data;
                        $scope.paginarConFiltro();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }

                $scope.buscar = function () {
                    $scope.nodoPin = [];
                    location.href = "http://" + config.ipFront + ":8017/menu.html#/:pagina=1";
                    if ($scope.filterText !== "" || $scope.placa !== "" || $scope.dependencia !== "" || $scope.edificio !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
//                    }
                };

                $scope.buscarSegunPagina = function () {
                    $scope.nodoPin = [];
                    if ($scope.filterText !== "" || $scope.placa !== "" || $scope.dependencia !== "" || $scope.edificio !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
                };

                //FIN DE PAGINACION



                $scope.onChanged = function () {
                    $scope.nestedItemsLevelPlacaPin = [];
                    $scope.idTipoPlacaPin = "";
                    NodoService.listarPorId(myMapNodo.get($scope.idNodo)).success(function (data) {
                        $scope.nodoPinAgregar.descriEdificio = data.edificio.descriEdificio;
                        $scope.nodoPinAgregar.descriPlaca = data.placa.descriPlaca;
                        $scope.nodoPinAgregar.descriTipoPlaca = data.placa.tipoPlaca.descriTipo;
                        $scope.nodoPinAgregar.descriDependencia = data.dependencia.descriDependencia;
                        $scope.nodoPinAgregar.descriComponente = data.componente.descriComponente;
//                        $scope.descripcionTipoComp = data.componente.descriComponente;
                        $("#descripcionTipoComp").val(data.componente.tipoComponente.descriTipoComp);
                        PlacaService.tipoPlacaPin(data.placa.idPlaca).success(function (d) {
                            angular.forEach(d, function (value, key) {
                                $scope.nestedItemsLevelPlacaPin.push(value.descriPin);
                                myMapPin.set(value.descriPin, value.idTipoPlacaPin);
                            });
                        });
                    });
                };

                $scope.agregar = function () {
                    $scope.nodoPinAgregar.nodo = {};
                    $scope.nodoPinAgregar.descriEdificio = "";
                    $scope.nodoPinAgregar.descriPlaca = "";
                    $scope.nodoPinAgregar.descriTipoPlaca = "";
                    $scope.nodoPinAgregar.descriDependencia = "";
                    $scope.nodoPinAgregar.descripcionTipoComp = "";
                    $scope.nodoPinAgregar.descriComponente = "";
                    $("#descripcionTipoComp").val("");
                    $scope.nodoPinAgregar.tipoPlacaPin = {};
                    myMapNodo = new Map();
                    myMapPin = new Map();
                    $scope.idNodo = "";
                    $scope.idTipoPlacaPin = "";
                    //PERSONA FILTRO
                    $scope.nestedItemsLevel1 = [];
//                    $scope.nestedItemsLevelPlacaPin = [];
                    NodoService.listarSinNodoPines().success(function (d) {
                        angular.forEach(d, function (value, key) {
                            $scope.nestedItemsLevel1.push(value.descriNodo);
                            myMapNodo.set(value.descriNodo, value.idNodo);
                        });
                    });
                };
                $scope.registrar = function () {
                    $scope.nodoPinAgregar.nodo.idNodo = myMapNodo.get($scope.idNodo);
                    $scope.nodoPinAgregar.descriComp = $scope.nodoPinAgregar.descriComponente;
                    $scope.nodoPinAgregar.tipoPlacaPin.idTipoPlacaPin = myMapPin.get($scope.idTipoPlacaPin);

                    NodoPinService.crear($scope.nodoPinAgregar).success(function (data) {
                        if (data === true) {
                            if ($("#descripcionTipoComp").val().toLowerCase() === "iluminacion") {
                                $('#modalAgregar').modal('hide');
                                $scope.onChanged();
                                $("#modalAgregarPulsador").modal('show');
                            } else {
                                swal("Mensaje del Sistema!", "Datos Agregados exitosamente", "success");
                                $('#modalAgregar').modal('hide');

                                $scope.listar();
                            }
                        } else {
                            swal("Mensaje del Sistema!", "Datos no Agregados, verique que los campos no estén vacíos", "error");
                        }
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };
                $scope.registrarPulsador = function () {
                    $scope.nodoPinAgregar.nodo.idNodo = myMapNodo.get($scope.idNodo);
                    $scope.nodoPinAgregar.descriComp = "PULSADOR";
                    $scope.nodoPinAgregar.tipoPlacaPin.idTipoPlacaPin = myMapPin.get($scope.idTipoPlacaPin);

                    NodoPinService.crear($scope.nodoPinAgregar).success(function (data) {
                        if (data === true) {
                            swal("Mensaje del Sistema!", "Datos Agregados exitosamente", "success");
                            $('#modalAgregarPulsador').modal('hide');

                            $scope.listar();
                        } else {
                            swal("Mensaje del Sistema!", "Datos no Agregados, verique que los campos no estén vacíos", "error");
                        }
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };
                $scope.atras = function () {
                    location.reload();
                };

                $scope.reporte = function () {
//                    $scope.valor = 0;
//                    if ($scope.filterText === "" || $scope.filterText === undefined) {
//                        $scope.valor++;
//                    }
//                    if ($scope.dependencia === "" || $scope.dependencia === undefined) {
//                        $scope.valor++;
//                    }
//                    if ($scope.placa === "" || $scope.placa === undefined) {
//                        $scope.valor++;
//                    }
//                    if ($scope.valor < 2) {
////                        alert($scope.valor);
//                        swal("Mensaje del Sistema!", "El filtro es por un solo campo", "error");
//                    } else {
                    $scope.jsonData = $cookies.getObject("usuarioLogueado");
                    $scope.userLog = $scope.jsonData.usuarioUsuario;

                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.dependencia === "" || $scope.dependencia === undefined) {
                        $scope.dep = null;
                    } else {
                        $scope.dep = $scope.dependencia;
                    }
                    if ($scope.placa === "" || $scope.placa === undefined) {
                        $scope.pla = null;
                    } else {
                        $scope.pla = $scope.placa;
                    }
                    if ($scope.edificio === "" || $scope.edificio === undefined) {
                        $scope.edi = null;
                    } else {
                        $scope.edi = $scope.edificio;
                    }

                    $scope.result = {};
                    $http.get(config.backend + '/nodoPin/reportarPorDescripcion/' + $scope.filt + '/' + $scope.dep + '/' + $scope.pla + '/' + $scope.edi + '/' + $scope.userLog, {responseType: 'arraybuffer'})
                            .success(function (data) {
                                var file = new Blob([data], {type: 'application/pdf'});
                                var fileURL = URL.createObjectURL(file);
                                window.open(fileURL);
                            });
//                    }
                };
//                $scope.eliminar = function (nodoPin) {
//                    var r = confirm("Seguro que quiere eliminar el Nodo " + nodoPin.nodo.descriNodo + "?");
//                    if (r === true) {
//                        NodoPinService.eliminar(nodoPin.idNodoPin).success(function (data) {
//                            if (data === true) {
//                                swal('Eliminado!',
//                                        'Haz eliminado exitosamente.',
//                                        'success'
//                                        );
//                                $scope.listar();
//                            } else {
//                                swal("Mensaje del Sistema!", "No se pudo eliminar Nodo Pin, verifique Dependencias", "error");
//                            }
//                        });
//                    } else {
//                        swal('Cancelado',
//                                'Haz cancelado la eliminación',
//                                'error'
//                                );
//                    }
//                };

                $scope.eliminar = function (nodoPin) {
                    swal({
                        title: "Mensaje del Sistema",
                        text: "Seguro que quiere eliminar el Nodo " + nodoPin.descriNodo + "?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Si",
                        cancelButtonText: "No",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                            function (isConfirm) {
                                if (isConfirm) {
                                    NodoPinService.eliminarPorNodo(nodoPin.idNodo).success(function (data) {
                                        if (data === true) {
                                            swal('Eliminado!',
                                                    'Haz eliminado exitosamente.',
                                                    'success'
                                                    );
                                            $scope.listar();
                                        } else {
                                            swal("Mensaje del Sistema!", "No se pudo eliminar Nodo Pin, verifique Dependencias", "error");
                                        }
                                    }).error(function (e) {
                                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                                    });
                                } else {
                                    swal("Mensaje del Sistema", "Haz cancelado la operación!", "error");
                                }
                            });
                };
                $scope.detalle = function (x) {
                    NodoPinService.getPines(x.idNodo).success(function (data) {
                        if (data.length !== 0) {
                            $scope.nodoPin = data;
                            $('#modalDetalles').modal('show');
                        } else {
                            swal("Mensaje del Sistema!", "No se encontraron resultados de la Búsqueda.", "error");
                        }
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }
                $scope.salir = function () {
                    $('#modalDetalles').modal('hide');
                }


                $scope.$watch('filterText', function () {
                    $("#paginaActual").val("1");
                })
                $scope.$watch('componente', function () {
                    $("#paginaActual").val("1");
                })
                $scope.$watch('dependencia', function () {
                    $("#paginaActual").val("1");
                })
                $scope.limpiar();
                $scope.buscar();
            }]);