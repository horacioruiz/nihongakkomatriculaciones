'use strict'
app
        .controller('listAndeCategoriaCtrl', ['$state', '$scope', '$http', 'config', 'AndeCategoriaService', '$cookies', '$base64', function ($state, $scope, $http, config, AndeCategoriaService, $cookies, $base64) {

                $scope.andeCategoria = {};
                $scope.andeCategoriaAgregar = {};
                $scope.andeCategoriaModificar = {};
                $("#paginaActual").val("1");
                $scope.totalItems = 0;
                $scope.currentPage = 0;
                $scope.pageChangedFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };
                $scope.pageChangedSinFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };

//                FIN DE PAGINACION

                $scope.listar = function () {
                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    AndeCategoriaService.fetch(config.cantPaginacion, offSet).success(function (data) {
                        $scope.andeCategoria = data;
                        $scope.paginar();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $scope.paginar = function () {
                    AndeCategoriaService.count().success(function (data) {
                        $scope.catidad = data;
                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "none");
                        $("#paginacionSinFiltro").css("display", "inline");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $scope.paginarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    AndeCategoriaService.countFiltro($scope.filt).success(function (data) {
                        $scope.catidad = data;

                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "inline");
                        $("#paginacionSinFiltro").css("display", "none");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };
                $scope.listarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }

                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    AndeCategoriaService.fetchFiltro(config.cantPaginacion, offSet, $scope.filt).success(function (data) {
                        $scope.andeCategoria = data;
                        $scope.paginarConFiltro();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $scope.generarReporte = function () {
                    $scope.jsonData = $cookies.getObject("usuarioLogueado");
                    $scope.userLog = $scope.jsonData.usuarioUsuario;

                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    $http.get(config.backend + '/andecategoria/generarReporte/' + $scope.filt + '/' + $scope.userLog, {responseType: 'arraybuffer'})
                            .success(function (data) {
                                var file = new Blob([data], {type: 'application/pdf'});
                                var fileURL = URL.createObjectURL(file);
                                window.open(fileURL);
                            });
                };

                $scope.buscar = function () {
                    $scope.andeCategoria = [];
                    if ($scope.filterText !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
                };
                $scope.buscarSegunPagina = function () {
                    $scope.andeCategoria = [];
                    if ($scope.filterText !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
                };

                $scope.registrar = function () {
                    $scope.jsonUsu = $cookies.getObject("usuarioLogueado");
                    $scope.andeCategoriaAgregar.catUsuAlta = $scope.jsonUsu.usuarioUsuario;
                    $scope.andeCategoriaAgregar.catUsuMod = $scope.jsonUsu.usuarioUsuario;
                    if ($scope.andeCategoriaAgregar.descriAndeCat === "" || $scope.andeCategoriaAgregar.descriAndeCat === undefined || $scope.andeCategoriaAgregar.descriAndeCat === null) {
                        swal("Mensaje del Sistema!", "Descripción no debe quedar vacío", "error");
                    } else {
                        AndeCategoriaService.crear($scope.andeCategoriaAgregar).success(function (data) {
                            if (data === true) {
                                swal("Mensaje del Sistema!", "Datos Agregados exitosamente", "success");
                                $('#modalAgregar').modal('hide');
                                $scope.andeCategoriaAgregar = {};
                                $scope.listar();
                            } else {
                                swal("Mensaje del Sistema!", "Datos no Agregados", "error");
                            }
                        }).error(function (e) {
                            swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                        });
                    }
                };

                $scope.atras = function () {
                    location.reload();
                };
                $scope.agregar = function () {
                    $scope.andeCategoriaAgregar.descriAndeCat = "";
                };

                $scope.actualizar = function () {
                    $scope.jsonUsu = $cookies.getObject("usuarioLogueado");
                    $scope.andeCategoriaModificar.catUsuMod = $scope.jsonUsu.usuarioUsuario;
                    if ($scope.andeCategoriaModificar.descriAndeCat === "" || $scope.andeCategoriaModificar.descriAndeCat === undefined || $scope.andeCategoriaModificar.descriAndeCat === null) {
                        swal("Mensaje del Sistema!", "Descripción no debe quedar vacío", "error");
                    } else {
                        AndeCategoriaService.actualizar($scope.andeCategoriaModificar).success(function (data) {
                            if (data === true) {
                                swal("Mensaje del Sistema!", "Datos Modificados exitosamente", "success");
                                $('#modalModificar').modal('hide');
                                $scope.andeCategoriaModificar = {};
                                $scope.listar();
                            } else {
                                swal("Mensaje del Sistema!", "Datos no Modificados", "error");
                            }
                        }).error(function (e) {
                            swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                        });
                    }
                };

                $scope.modificar = function (aCat) {
                    $scope.andeCategoriaModificar.descriAndeCat = aCat.descriAndeCat;
                    $scope.andeCategoriaModificar.idAndeCat = aCat.idAndeCat;
                    $("#modalModificar").modal("show");
                };

                $scope.eliminar = function (marc) {
                    $scope.datoEliminar = {};
                    $scope.datoEliminar.idAndeCat = marc.idAndeCat;
                    swal({
                        title: "Mensaje del Sistema",
                        text: "Seguro que quiere eliminar " + marc.descriAndeCat + "?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Si",
                        cancelButtonText: "No",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                            function (isConfirm) {
                                if (isConfirm) {
                                    AndeCategoriaService.eliminar(marc.idAndeCat).success(function (data) {
//                                    AndeCategoriaService.modEliminar($scope.datoEliminar).success(function (data) {
                                        if (data === true) {
                                            swal("Mensaje del Sistema!", "Datos Modificados exitosamente", "success");
                                            $scope.andeCategoriaModificar = {};
                                            $scope.listar();
                                        } else {
                                            swal("Mensaje del Sistema!", "No se pudo eliminar la Categoría, verifique Dependencias", "error");
                                        }
                                    }).error(function (e) {
                                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                                    });
                                } else {
                                    swal("Mensaje del Sistema", "Haz cancelado la operación!", "error");
                                }
                            });
                };

                $scope.listar();
            }]);