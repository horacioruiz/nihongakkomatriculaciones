'use strict'
app
        .factory('AccesoFingerService', function ($http, config) {
            var url = config.backend + "/accesoFinger";
            return {
                listar: function () {
                    return $http.get(url);
                },
                listarPorId: function (id) {
                    return $http.get(url + "/" + id);
                },
                crear: function (data) {
                    return $http.post(url, data);
                },
                actualizar: function (data) {
                    return $http.put(url, data);
                },
                eliminar: function (id) {
                    return $http.delete(url + "/" + id);
                },
                fetch: function (limit, offset) {
                    return $http.get(url + "/fetch/" + limit + "/" + offset);
                },
                fetchFiltro: function (limit, offset, usu, fecha, edificio) {
                    return $http.get(url + "/fetchFiltro/" + limit + "/" + offset + "/" + usu + "/" + fecha + "/" + edificio);
                },
                count: function () {
                    return $http.get(url + "/count/");
                },
                countFiltro: function (usu, fecha, edificio) {
                    return $http.get(url + "/countFiltro/" + usu + "/" + fecha + "/" + edificio);
                },
                reportarPorDescripcion: function (usu, fecha, edi, usuario) {
                    return $http.get(url + "/reportarPorDescripcion/" + usu + "/" + fecha + "/" + edi + "/" + usuario);
                }
            };
        });