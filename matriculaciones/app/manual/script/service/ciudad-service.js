'use strict'
app
        .factory('CiudadService', function ($http, config) {
            var url = config.backend + "/ciudad";
            return {
                listar: function () {
                    return $http.get(url);
                },
                listarPorId: function (id) {
                    return $http.get(url + "/" + id);
                },
                fetch: function (limit, offset) {
                    return $http.get(url + "/fetch/" + limit + "/" + offset);
                },
                fetchFiltro: function (limit, offset, descr) {
                    return $http.get(url + "/fetchFiltro/" + limit + "/" + offset + "/" + descr);
                },
                count: function () {
                    return $http.get(url + "/count/");
                },
                countFiltro: function (descr) {
                    return $http.get(url + "/countFiltro/" + descr);
                },
                crear: function (data) {
                    return $http.post(url, data);
                },
                actualizar: function (data) {
                    return $http.put(url, data);
                },
                eliminar: function (id) {
                    return $http.delete(url + "/" + id);
                },
                modEliminar: function (data) {
                    return $http.put(url + "/eliminar", data);
                }
            };
        });