'use strict'
app
        .factory('PlacaPinService', function ($http, config) {
            var url = config.backend + "/placaPin";
            return {
                listar: function () {
                    return $http.get(url);
                },
                listarPorId: function (id) {
                    return $http.get(url + "/" + id);
                },
                crear: function (data) {
                    return $http.post(url, data);
                },
                fetch: function (limit, offset) {
                    return $http.get(url + "/fetch/" + limit + "/" + offset);
                },
                fetchFiltro: function (limit, offset, placa, tipo) {
                    return $http.get(url + "/fetchFiltro/" + limit + "/" + offset + "/" + placa + "/" + tipo);
                },
                count: function () {
                    return $http.get(url + "/count/");
                },
                countFiltro: function (placa, tipo) {
                    return $http.get(url + "/countFiltro/" + placa + "/" + placa);
                },
                actualizar: function (data) {
                    return $http.put(url, data);
                },
                eliminar: function (id) {
                    return $http.delete(url + "/" + id);
                },
                modEliminar: function (data) {
                    return $http.put(url + "/eliminar", data);
                },
                reportarPorDescripcion: function (placa, tipo, usuario) {
                    return $http.get(url + "/reportarPorDescripcion/" + placa + "/" + tipo + "/" + usuario);
                }
            };
        });