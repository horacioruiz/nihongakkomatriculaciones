'use strict'
appLogin
        .factory('UsuarioService', function ($http, config) {
            var url = config.backend + "/usuarios";
            var urlMoodle = "https://nihongakko.com/login/token.php?service=curs";
            return {
                listar: function () {
                    return $http.get(url);
                },
                listarPorId: function (id) {
                    return $http.get(url + "/" + id);
                },
                getAllConfirm: function () {
                    return $http.get(url + "/getAllConfirm");
                },
                verificarUsuarioExistencia: function (user) {
                    return $http.get(url + "/userExist/" + user);
                },
                verificarEmailExistencia: function (user) {
                    return $http.get(url + "/emailExist/" + user);
                },
                getIdByUser: function (user) {
                    return $http.get(url + "/userReturnId/" + user);
                },
                generarReporte: function (usu, nom, ape, usuLogin) {
                    return $http.get(url + "/generarReporte/" + usu + "/" + nom + "/" + ape + "/" + usuLogin);
                },
                restaurarPass: function (id) {
                    return $http.get(url + "/restaurarPass/" + id);
                },
                restablecerPass: function (id) {
                    return $http.get(url + "/restablecerPass/" + id);
                },
                login: function (usuario, clave) {
                    return $http.get(url + "/" + usuario + "/" + clave);
                },
                loginMoodle: function (usuario, clave) {alert(clave)
                    return $http.get(urlMoodle + "&username=" + usuario + "&password=" + clave);
                },
                crear: function (data) {
                    return $http.post(url, data);
                },
                actualizar: function (data) {
                    return $http.put(url, data);
                },
                eliminar: function (id) {
                    return $http.delete(url + "/" + id);
                }
            };
        });