'use strict'
app
        .factory('RolService', function ($http, config) {
            var url = config.backend + "/roles";
            return {
                listar: function () {
                    return $http.get(url);
                },
                listarPorId: function (id) {
                    return $http.get(url + "/" + id);
                },
                crear: function (data) {
                    return $http.post(url, data);
                },
                crearAdd: function (data) {
                    return $http.post(url + "/add", data);
                },
                actualizar: function (data) {
                    return $http.put(url, data);
                },
                update: function (id, descri) {
                    return $http.get(url + "/update/" + id + "/" + descri);
                },
                eliminar: function (id) {
                    return $http.delete(url + "/" + id);
                },
                deleteUpdate: function (id) {
                    return $http.get(url + "/eliminar/" + id);
                },
                fetch: function (limit, offset) {
                    return $http.get(url + "/fetch/" + limit + "/" + offset);
                },
                fetchFiltro: function (limit, offset, descr) {
                    return $http.get(url + "/fetchFiltro/" + limit + "/" + offset + "/" + descr);
                },
                count: function () {
                    return $http.get(url + "/count/");
                },
                countFiltro: function (descr) {
                    return $http.get(url + "/countFiltro/" + descr);
                }
            };
        });