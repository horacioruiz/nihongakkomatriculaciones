'use strict'
app
        .factory('NodoService', function ($http, config) {
            var url = config.backend + "/nodo";
            return {
                listar: function () {
                    return $http.get(url);
                },
                listarPorId: function (id) {
                    return $http.get(url + "/" + id);
                },
                listarPorEdificio: function (id) {
                    return $http.get(url + "/listarPorEdificio/" + id);
                },
                listarSensoresDescri: function (id, humo, humedad, mov) {
                    return $http.get(url + "/listarSensoresDescri/" + id + "/" + humo + "/" + humedad + "/" + mov);
                },
                listarSensoresTemp: function (id) {
                    return $http.get(url + "/listarSensoresTemp/" + id);
                },
                listarSinNodoPines: function () {
                    return $http.get(url + "/listarSinNodoPines");
                },
                listarPorSensores: function () {
                    return $http.get(url + "/listarPorSensores");
                },
                listarPorEdiDepe: function (depe, edi) {
                    return $http.get(url + "/listarPorEdiDepe/" + depe + "/" + edi);
                },
                getNodoByControlAcceso: function (idCompControl, idControl) {
                    return $http.get(url + "/getNodoByControlAcceso/" + idCompControl + "/" + idControl);
                },
                crear: function (data) {
                    return $http.post(url, data);
                },
                crearRecuperarId: function (data) {
                    return $http.post(url + "/crearRecuperarId", data);
                },
                actualizar: function (data) {
                    return $http.put(url, data);
                },
                getByCantidadDependencia: function (id) {
                    return $http.get(url + "/getByCantidadDependencia/" + id);
                },
                getNodoByAdministracion: function () {
                    return $http.get(url + "/getNodoByAdministracion");
                },
                listarPorEdificioDependencia: function (idEdi, idDepe) {
                    return $http.get(url + "/listarPorEdificioDependencia/" + idEdi + "/" + idDepe);
                },
                getByEdificio: function (id) {
                    return $http.get(url + "/getByEdificio/" + id);
                },
                getByEdificiOnlyComp: function (id) {
                    return $http.get(url + "/getByEdificiOnlyComp/" + id);
                },
                bajas: function (data) {
                    return $http.put(url + "/bajas", data);
                },
                cambEstadoActuador: function (idNodo) {
                    return $http.get(url + "/cambEstadoActuador/" + idNodo);
                },
                getEstado: function (idNodo) {
                    return $http.get(url + "/estadoActual/" + idNodo);
                },
                eliminar: function (id) {
                    return $http.delete(url + "/" + id);
                },
                modEliminar: function (data) {
                    return $http.put(url + "/eliminar", data);
                },
                fetch: function (limit, offset) {
                    return $http.get(url + "/fetch/" + limit + "/" + offset);
                },
                fetchNodoPin: function (limit, offset) {
                    return $http.get(url + "/fetchNodoPin/" + limit + "/" + offset);
                },
                listarPorEdificioComponenteAcceso: function (idEdificio, idComp) {
                    return $http.get(url + "/listarPorEdificioComponenteAcceso/" + idEdificio + "/" + idComp);
                },
                getEdificioDependeciaByControlAcceso: function (idCompAcceso) {
                    return $http.get(url + "/getEdificioDependeciaByControlAcceso/" + idCompAcceso);
                },
                fetchFiltro: function (limit, offset, descri, comp, dep, placa, edi) {
                    return $http.get(url + "/fetchFiltro/" + limit + "/" + offset + "/" + descri + "/" + comp + "/" + dep + "/" + placa + "/" + edi);
                },
                fetchFiltroNodoPin: function (limit, offset, descri, comp, dep, placa, edi) {
                    return $http.get(url + "/fetchFiltroNodoPin/" + limit + "/" + offset + "/" + descri + "/" + comp + "/" + dep + "/" + placa + "/" + edi);
                },
                count: function () {
                    return $http.get(url + "/count/");
                },
                countNodoPin: function () {
                    return $http.get(url + "/countNodoPin/");
                },
                countFiltro: function (descri, comp, dep, placa, edi) {
                    return $http.get(url + "/countFiltro/" + descri + "/" + comp + "/" + dep + "/" + placa + "/" + edi);
                },
                countFiltroNodoPin: function (descri, comp, dep, placa, edi) {
                    return $http.get(url + "/countFiltroNodoPin/" + descri + "/" + comp + "/" + dep + "/" + placa + "/" + edi);
                }
            };
        });