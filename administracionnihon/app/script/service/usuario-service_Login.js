'use strict'
appLogin
        .factory('UsuarioService', function ($http, config) {
            var url = config.backend + "/usuarios";
            var urlMoodle = "https://nihongakko.com/login/token.php?service=curs";
            return {
                listar: function () {
                    return $http.get(url);
                },
                traerMatriculacionesPendientes: function () {
                    return $http.get("http://" + config.ipBackSpring + ":8885/nihon/matriculaciones/");
                },
                listarPorId: function (id) {
                    return $http.get(url + "/" + id);
                },
                listarCategoriasFilter: function (id) {
                    return $http.get("https://nihongakko.com/webservice/rest/server.php?wstoken=2366ace0549fd307e165484b450cb320&wsfunction=core_course_get_categories&moodlewsrestformat=json&criteria[0][key]=parent&criteria[0][value]=" + id);
                },
                getAllConfirm: function () {
                    return $http.get(url + "/getAllConfirm");
                },
                verificarUsuarioExistencia: function (user) {
                    return $http.get(url + "/userExist/" + user);
                },
                verificarEmailExistencia: function (user) {
                    return $http.get(url + "/emailExist/" + user);
                },
                getIdByUser: function (user) {
                    return $http.get(url + "/userReturnId/" + user);
                },
                generarReporte: function (usu, nom, ape, usuLogin) {
                    return $http.get(url + "/generarReporte/" + usu + "/" + nom + "/" + ape + "/" + usuLogin);
                },
                restaurarPass: function (id) {
                    return $http.get(url + "/restaurarPass/" + id);
                },
                restablecerPass: function (id) {
                    return $http.get(url + "/restablecerPass/" + id);
                },
                login: function (usuario, clave) {
                    return $http.get(url + "/" + usuario + "/" + clave);
                },
                loginMoodle: function (usuario, clave) {
                    if(clave.includes("%")){
                        clave = clave.replace("%", "%25");
                        }
                    if(clave.includes("#")){
                        clave = clave.replace("#", "%23");
                        }
                         if(clave.includes("*")){
                        clave = clave.replace("*", "%2A");
                        }
                        if(clave.includes("+")){
                        clave = clave.replace("+", "%2B");
                        }
                        if(clave.includes("-")){
                        clave = clave.replace("-", "%2D");
                        }
                        if(clave.includes("&")){
                        clave = clave.replace("&", "%26");
                        }
                        if(clave.includes("$")){
                        clave = clave.replace("%", "%24");
                        }
                        if(clave.includes("!")){
                        clave = clave.replace("!", "%21");
                        }
                    return $http.get(urlMoodle + "&username=" + usuario + "&password=" + clave);
                },
                recuperarUltimoPeriodo: function () {
                    return $http.get("http://" + config.ipBackSpring + ":8885/nihon/periodo/ultimo");
                },
                crear: function (data) {
                    return $http.post(url, data);
                },
                actualizar: function (data) {
                    return $http.put(url, data);
                },
                eliminar: function (id) {
                    return $http.delete(url + "/" + id);
                }
            };
        });