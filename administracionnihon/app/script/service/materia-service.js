'use strict'
app
        .factory('MateriaService', function ($http, config) {
            var url = "https://nihongakko.com/webservice/rest/server.php?wstoken=2366ace0549fd307e165484b450cb320&wsfunction=core_course_get_courses&moodlewsrestformat=json";
            return {
                listar: function () {
                    return $http.get(url);
                },
                traerMatriculacionesPendientes: function (periodo) {
                    return $http.get("http://" + config.ipBackSpring + ":8885/nihon/matriculaciones/pendientes/" + periodo);
                },
                cargarPeriodo: function (data) {
                    return $http.post("http://" + config.ipBackSpring + ":8885/nihon/periodo/", data);
                },
                recuperarPeriodo: function () {
                    return $http.get("http://" + config.ipBackSpring + ":8885/nihon/periodo/");
                },
                listarCategorias: function () {
                    return $http.get("https://nihongakko.com/webservice/rest/server.php?wstoken=2366ace0549fd307e165484b450cb320&wsfunction=core_course_get_categories&moodlewsrestformat=json");
                },
                listaDeUsuariosPorUsername: function (username) {
                    return $http.get("https://nihongakko.com/webservice/rest/server.php?wstoken=2366ace0549fd307e165484b450cb320&wsfunction=core_user_get_users_by_field&moodlewsrestformat=json&field=username&values[0]=" + username);
                },
                listarCategoriasFilter: function (id) {
                    return $http.get("https://nihongakko.com/webservice/rest/server.php?wstoken=2366ace0549fd307e165484b450cb320&wsfunction=core_course_get_categories&moodlewsrestformat=json&criteria[0][key]=parent&criteria[0][value]=" + id);
                },
                listarPorId: function (id) {
                    return $http.get(url + "/" + id);
                },
                traerMaterias: function () {
                    return $http.get("http://" + config.ipBackSpring + ":8885/nihon/jsonmaterias/1");
                },
                generarReporte: function (ci, nom, ape, usuLogin) {
                    return $http.get(url + "/generarReporte/" + ci + "/" + nom + "/" + ape + "/" + usuLogin);
                },
                crear: function (data) {
                    return $http.post("https://nihongakko.com/webservice/rest/server.php?wstoken=2366ace0549fd307e165484b450cb320&wsfunction=core_course_create_courses&moodlewsrestformat=json&" + data);
                },
                registrarLocalmente: function (data) {
                    return $http.post("http://" + config.ipBackSpring + ":8885/nihon/matriculaciones/", data);
                },
                actualizarLocalmente: function (data) {
                    return $http.put("http://" + config.ipBackSpring + ":8885/nihon/matriculaciones/", data);
                },
                quitarLocalmente: function (idusu, idmate) {
                    return $http.get("http://" + config.ipBackSpring + ":8885/nihon/matriculaciones/quitarMatriculacion/" + idusu + "/" + idmate);
                },
                listarLocalmentePorUsuarioEstado: function (id, estado) {
                    return $http.get("http://" + config.ipBackSpring + ":8885/nihon/matriculaciones/matriculacionesByIdUsuarioEstado/" + id + "/" + estado);
                },
                listarUsuarios: function (data) {
                    return $http.get("https://nihongakko.com/webservice/rest/server.php?wstoken=2366ace0549fd307e165484b450cb320&wsfunction=core_user_get_users&moodlewsrestformat=json&criteria[0][key]=email&criteria[0][value]=%%");
                },
                enrolar: function (data) {
                    return $http.get("https://nihongakko.com/webservice/rest/server.php?wstoken=2366ace0549fd307e165484b450cb320&moodlewsrestformat=json&wsfunction=enrol_manual_enrol_users&" + data);
                },
                actualizar: function (data) {
                    return $http.post("https://nihongakko.com/webservice/rest/server.php?wstoken=2366ace0549fd307e165484b450cb320&wsfunction=core_course_update_courses&moodlewsrestformat=json&" + data);
                },
                actualizarNada: function (data) {
                    return $http.put(url, data);
                },
                eliminar: function (id) {
                    return $http.delete(url + "/" + id);
                },
                fetch: function (limit, offset) {
                    return $http.get(url + "/fetch/" + limit + "/" + offset);
                },
                fetchFiltro: function (limit, offset, nom, ape, ci) {
                    return $http.get(url + "/fetchFiltro/" + limit + "/" + offset + "/" + nom + "/" + ape + "/" + ci);
                },
                count: function () {
                    return $http.get(url + "/count/");
                },
                countFiltro: function (nom, ape, ci) {
                    return $http.get(url + "/countFiltro/" + nom + "/" + ape + "/" + ci);
                }
            };
        });