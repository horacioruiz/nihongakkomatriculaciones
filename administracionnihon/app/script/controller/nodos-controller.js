'use strict'
app
        .controller('listNodoCtrl', ['$state', '$scope', 'config', 'NodoService', '$cookies', '$base64', 'ComponenteService', 'DependenciaService', 'EdificioService', 'EntidadService', 'PlacaService', '$http', '$rootScope', 'TipoEdificioDepeService', 'NodoPinService', function ($state, $scope, config, NodoService, $cookies, $base64, ComponenteService, DependenciaService, EdificioService, EntidadService, PlacaService, $http, $rootScope, TipoEdificioDepeService, NodoPinService) {
                $scope.nodos = {};
                $scope.nodoAgregar = {};
                $scope.nodoModificar = {};

                $scope.nodoAgregar.componente = {};
                $scope.nodoAgregar.dependencia = {};
                $scope.nodoAgregar.placa = {};
                $scope.nodoAgregar.edificio = {};
                $scope.nodoAgregar.entidad = {};

                $scope.nodoModificar.componente = {};
                $scope.nodoModificar.dependencia = {};
                $scope.nodoModificar.placa = {};
                $scope.nodoModificar.edificio = {};
                $scope.nodoModificar.entidad = {};
                var myMapPin = new Map();

                $scope.nestedItemsLevelDependencia = [];

                $("#paginaActual").val("1");
                $scope.totalItems = 0;
                $scope.currentPage = 0;
                $scope.pageChangedFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };
                $scope.pageChangedSinFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };

                $scope.limpiar = function () {
                    $scope.filterText = "";
                    $scope.dependencia = "";
                    $scope.componente = "";
                    $scope.placa = "";
                    $scope.edificio = "";
                };

                $scope.aModificar = "";
                $scope.idNodoDato = 0;

//               pagination
                $scope.paginar = function () {
                    NodoService.count().success(function (data) {
                        $scope.catidad = data;

                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "none");
                        $("#paginacionSinFiltro").css("display", "inline");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $rootScope.$on("$locationChangeStart", function (event, data) {
                    var pag = parseInt(data.replace("http://" + config.ipFront + ":8017/menu.html#/:pagina=", ""));
                    if (!isNaN(pag)) {
                        $("#paginaActual").val(pag);
                        $scope.buscarSegunPagina();
                    }
                });

                $scope.paginarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.componente === "" || $scope.componente === undefined) {
                        $scope.comp = null;
                    } else {
                        $scope.comp = $scope.componente;
                    }
                    if ($scope.dependencia === "" || $scope.dependencia === undefined) {
                        $scope.dep = null;
                    } else {
                        $scope.dep = $scope.dependencia;
                    }
                    if ($scope.placa === "" || $scope.placa === undefined) {
                        $scope.pla = null;
                    } else {
                        $scope.pla = $scope.placa;
                    }
                    if ($scope.edificio === "" || $scope.edificio === undefined) {
                        $scope.edi = null;
                    } else {
                        $scope.edi = $scope.edificio;
                    }
                    NodoService.countFiltro($scope.filt, $scope.comp, $scope.dep, $scope.pla, $scope.edi).success(function (data) {
                        $scope.catidad = data;
                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());

                        $("#paginacionFiltro").css("display", "inline");
                        $("#paginacionSinFiltro").css("display", "none");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $scope.generarReporte = function () {
                    $scope.jsonData = $cookies.getObject("usuarioLogueado");
                    $scope.userLog = $scope.jsonData.usuarioUsuario;

                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.componente === "" || $scope.componente === undefined) {
                        $scope.comp = null;
                    } else {
                        $scope.comp = $scope.componente;
                    }
                    if ($scope.dependencia === "" || $scope.dependencia === undefined) {
                        $scope.dep = null;
                    } else {
                        $scope.dep = $scope.dependencia;
                    }
                    if ($scope.placa === "" || $scope.placa === undefined) {
                        $scope.pla = null;
                    } else {
                        $scope.pla = $scope.placa;
                    }
                    if ($scope.edificio === "" || $scope.edificio === undefined) {
                        $scope.edi = null;
                    } else {
                        $scope.edi = $scope.edificio;
                    }
                    $http.get(config.backend + '/nodo/generarReporte/' + $scope.filt + '/' + $scope.edi + '/' + $scope.dep + '/' + $scope.comp + '/' + $scope.pla + '/' + $scope.userLog, {responseType: 'arraybuffer'})
                            .success(function (data) {
                                var file = new Blob([data], {type: 'application/pdf'});
                                var fileURL = URL.createObjectURL(file);
                                window.open(fileURL);
                            });
                };

                $scope.listarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.componente === "" || $scope.componente === undefined) {
                        $scope.comp = null;
                    } else {
                        $scope.comp = $scope.componente;
                    }
                    if ($scope.dependencia === "" || $scope.dependencia === undefined) {
                        $scope.dep = null;
                    } else {
                        $scope.dep = $scope.dependencia;
                    }
                    if ($scope.placa === "" || $scope.placa === undefined) {
                        $scope.pla = null;
                    } else {
                        $scope.pla = $scope.placa;
                    }
                    if ($scope.edificio === "" || $scope.edificio === undefined) {
                        $scope.edi = null;
                    } else {
                        $scope.edi = $scope.edificio;
                    }
                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    NodoService.fetchFiltro(config.cantPaginacion, offSet, $scope.filt, $scope.comp, $scope.dep, $scope.pla, $scope.edi).success(function (data) {
                        $scope.nodos = data;
                        $scope.paginarConFiltro();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }

                $scope.buscar = function () {
//                    alert("HOLAAAA ->> "+$location.url());
                    $scope.nodos = [];
                    location.href = "http://" + config.ipFront + ":8017/menu.html#/:pagina=1";
                    if ($scope.filterText !== "" || $scope.componente !== "" || $scope.dependencia !== "" || $scope.placa !== "" || $scope.edificio !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
                };

                $scope.buscarSegunPagina = function () {
                    $scope.nodos = [];
                    if ($scope.filterText !== "" || $scope.componente !== "" || $scope.dependencia !== "" || $scope.placa !== "" || $scope.edificio !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
                };

//                FIN DE PAGINACION

                var myMap = new Map();
                var myMapEdificio = new Map();
                var myMapPlaca = new Map();
                var myMapDependencia = new Map();
                var myMapComponente = new Map();

                $scope.listar = function () {
                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    NodoService.fetch(config.cantPaginacion, offSet).success(function (data) {
                        $scope.nodos = data;
                        $scope.paginar();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };
                $scope.onChanged = function () {
                    myMapPin = new Map();
                    $scope.nestedItemsLevelPlacaPin = [];
                    $scope.idTipoPlacaPin = "";
                    NodoService.listarPorId($scope.idNodoDato).success(function (data) {
                        $("#descripcionTipoComp").val(data.componente.tipoComponente.descriTipoComp);
                        PlacaService.tipoPlacaPin(data.placa.idPlaca).success(function (d) {
                            angular.forEach(d, function (value, key) {
                                $scope.nestedItemsLevelPlacaPin.push(value.descriPin);
                                myMapPin.set(value.descriPin, value.idTipoPlacaPin);
                            });
                        });
                    });
                };
                $scope.detallePines = function (x) {
                    NodoPinService.getPines(x.idNodo).success(function (data) {
                        if (data.length !== 0) {
                            $scope.nodoPin = data;
                            $('#modalDetalles').modal('show');
                        } else {
                            swal("Mensaje del Sistema!", "No se encontraron resultados de la Búsqueda.", "error");
                        }
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }
                $scope.salirPines = function () {
                    $('#modalDetalles').modal('hide');
                }
                $scope.registrar = function () {
                    $scope.nodoAgregar.componente = {};
                    $scope.nodoAgregar.dependencia = {};
                    $scope.nodoAgregar.placa = {};
                    $scope.nodoAgregar.edificio = {};
                    $scope.nodoAgregar.entidad = {};

                    if ($scope.nodoAgregar.descriNodo === "" || $scope.nodoAgregar.descriNodo === undefined || $scope.nodoAgregar.descriNodo === null) {
                        swal("Mensaje del Sistema!", "Descripción no debe quedar vacío", "error");
                    } else {
                        var val = false;
                        angular.forEach($scope.nodos, function (value, key) {
                            if (value.descriNodo.toString().toUpperCase() === $scope.nodoAgregar.descriNodo.toString().toUpperCase()) {
                                val = true;
                            }
                        });

                        if (val) {
                            swal("Mensaje del Sistema!", "La descripción de Nodo no debe coincidir con otro", "error");
                        } else {
                            $scope.nodoAgregar.componente.idComponente = myMapComponente.get($scope.idComponente);
                            $scope.nodoAgregar.dependencia.idDependencia = myMapDependencia.get($scope.idDependencia);
                            $scope.nodoAgregar.placa.idPlaca = myMapPlaca.get($scope.idPlaca);
                            $scope.nodoAgregar.edificio.idEdificio = myMapEdificio.get($scope.idEdificio);
                            $scope.nodoAgregar.entidad.idEntidad = 1;
//                        $scope.nodoAgregar.entidad.idEntidad = myMap.get($scope.idEntidad);
                            $scope.nodoAgregar.estadoComponente = true;

                            $scope.jsonUsu = $cookies.getObject("usuarioLogueado");
                            $scope.nodoAgregar.usuAltaNodo = $scope.jsonUsu.usuarioUsuario;
                            $scope.nodoAgregar.usuModNodo = $scope.jsonUsu.usuarioUsuario;
                            NodoService.crearRecuperarId($scope.nodoAgregar).success(function (data) {
                                if (parseInt(data.placa.idPlaca) !== 0) {
                                    $("#descripcionTipoComp").val(data.componente.tipoComponente.descriTipoComp);
//                                swal("Mensaje del Sistema!", "Datos Agregados exitosamente", "success");
                                    $('#modalAgregar').modal('hide');
//                                $scope.nestedItemsLevelPlacaPin = [];
//                                $scope.idTipoPlacaPin = "";
                                    if ($("#descripcionTipoComp").val().toLowerCase() === "iluminacion" ||
                                            $("#descripcionTipoComp").val().toLowerCase() === "ventilador" ||
                                            $("#descripcionTipoComp").val().toLowerCase() === "puerta" ||
                                            $("#descripcionTipoComp").val().toLowerCase() === "sensor" ||
                                            $("#descripcionTipoComp").val().toLowerCase() === "alarma" ||
                                            $("#descripcionTipoComp").val().toLowerCase() === "riego") {
                                        $scope.idNodoDato = parseInt(data.idNodo);
//                                PlacaService.tipoPlacaPin(parseInt(data.placa.idPlaca)).success(function (d) {
//                                    angular.forEach(d, function (value, key) {
//                                        $scope.nestedItemsLevelPlacaPin.push(value.descriPin);
//                                        myMapPin.set(value.descriPin, value.idTipoPlacaPin);
                                        $scope.onChanged();
                                        $('#modalAgregarNodoPin').modal('show');
                                    } else {
                                        $scope.listar();
                                    }

//                                    });
//                                });

//                                $scope.listar();
                                } else {
                                    swal("Mensaje del Sistema!", "Datos no Agregados, verifique que los campos no estén vacíos o Cambia la descripción del Nodo.", "error");
                                }
                            }).error(function (e) {
                                swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                            });
                        }
                    }
                };

                $scope.registrarPulsador = function () {
                    $scope.nodoPinAgregar.nodo.idNodo = $scope.idNodoDato;
                    $scope.nodoPinAgregar.descriComp = "PULSADOR";
                    $scope.nodoPinAgregar.tipoPlacaPin.idTipoPlacaPin = myMapPin.get($scope.idTipoPlacaPin);

                    NodoPinService.crear($scope.nodoPinAgregar).success(function (data) {
                        if (data === true) {
                            swal("Mensaje del Sistema!", "Datos Agregados exitosamente", "success");
                            $('#modalAgregarPulsador').modal('hide');

                            $scope.listar();
                        } else {
                            swal("Mensaje del Sistema!", "Datos no Agregados, verique que los campos no estén vacíos", "error");
                        }
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $scope.registrarNodoPin = function () {
                    $scope.nodoPinAgregar = {};
                    $scope.nodoPinAgregar.nodo = {};
                    $scope.nodoPinAgregar.tipoPlacaPin = {};
                    $scope.nodoPinAgregar.nodo.idNodo = $scope.idNodoDato;
                    $scope.nodoPinAgregar.descriComp = $scope.idComponente;
                    $scope.nodoPinAgregar.tipoPlacaPin.idTipoPlacaPin = myMapPin.get($scope.idTipoPlacaPin);

                    NodoPinService.crear($scope.nodoPinAgregar).success(function (data) {
                        if (data === true) {
                            if ($("#descripcionTipoComp").val().toLowerCase() === "iluminacion") {
                                $('#modalAgregarNodoPin').modal('hide');
//                                $scope.nestedItemsLevelPlacaPin = [];
//                                $scope.idTipoPlacaPin = "";
                                $scope.onChanged();
//                                COMENTADO LO DE ABAJO
//                                $("#modalAgregarPulsador").modal('show');
                            } else {
                                swal("Mensaje del Sistema!", "Datos Agregados exitosamente", "success");
                                $('#modalAgregarNodoPin').modal('hide');
                                $('#modalAgregar').modal('hide');

//                                $scope.listar();
                            }
                        } else {
                            swal("Mensaje del Sistema!", "Datos no Agregados, verique que los campos no estén vacíos", "error");
                        }
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $scope.atras = function () {
                    location.reload();
                };

                //SEPARADOR DE MILES

                $('input.number').keyup(function (event) {

                    // skip for arrow keys
                    if (event.which >= 37 && event.which <= 40) {
                        event.preventDefault();
                    }

                    $(this).val(function (index, value) {
                        return value
                                .replace(/\D/g, "")
                                //.replace(/([0-9])([0-9]{2})$/, '$1.$2')  
                                .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
                    });
                });

                //FIN DE SEPARADOR DE MILES


                $scope.actualizar = function () {

                    $scope.jsonUsu = $cookies.getObject("usuarioLogueado");
                    $scope.nodoModificar.usuModNodo = $scope.jsonUsu.usuarioUsuario;

                    if ($scope.nodoModificar.descriNodo === "" || $scope.nodoModificar.descriNodo === undefined || $scope.nodoModificar.descriNodo === null) {
                        swal("Mensaje del Sistema!", "Descripción no debe quedar vacío", "error");
                    } else {
//                        NodoService.actualizar($scope.nodoModificar)
                        NodoService.eliminar($scope.nodoModificar.idNodo).success(function (dataVal) {
                            if (dataVal === true) {
                                swal("Mensaje del Sistema!", "Datos Eliminados Satisfactoriamente!", "success");
                                $('#modalModificar').modal('hide');
//                            $scope.nodoModificar = {};
                                $scope.listar();
                            } else {
                                NodoService.actualizar($scope.nodoModificar).success(function (data) {
                                    if (data === true) {
                                        swal("Mensaje del Sistema!", "Datos Modificados exitosamente", "success");
                                        $('#modalModificar').modal('hide');
//                            $scope.nodoModificar = {};
                                        $scope.listar();
                                    } else {
                                        swal("Mensaje del Sistema!", "Datos no Modificados", "error");
                                    }
                                }).error(function (e) {
                                    swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                                });
                            }
                        });
                    }
                };

                $scope.agregar = function () {
                    //PERSONA FILTRO
                    $scope.nodoAgregar = {};
                    $scope.idComponente = "";
                    $scope.idDependencia = "";
                    $scope.idPlaca = "";
                    $scope.idEdificio = "";
                    $scope.idEntidad = "";


                    $scope.nestedItemsLevel1 = []; //ENTIDAD
                    $scope.nestedItemsLevelEdificio = [];
                    $scope.nestedItemsLevelPlaca = [];
                    $scope.nestedItemsLevelDependencia = [];
                    $scope.nestedItemsLevelComponente = [];

//                    PlacaService.listarPlacaNodoTipoCompo().success(function (d) {
//                        angular.forEach(d, function (value, key) {
//                            $scope.nestedItemsLevelPlaca.push(value.descriPlaca);
//                            myMapPlaca.set(value.descriPlaca, value.idPlaca);
//                        });
//                    });
                    PlacaService.listar().success(function (d) {
                        angular.forEach(d, function (value, key) {
                            $scope.nestedItemsLevelPlaca.push(value.descriPlaca);
                            myMapPlaca.set(value.descriPlaca, value.idPlaca);
                        });
                    });
                    EntidadService.listar().success(function (d) {
                        angular.forEach(d, function (value, key) {
                            $scope.nestedItemsLevel1.push(value.descriEntidad);
                            myMap.set(value.descriEntidad, value.idEntidad);
                        });
                    });
                    EdificioService.listar().success(function (d) {
                        angular.forEach(d, function (value, key) {
                            $scope.nestedItemsLevelEdificio.push(value.descriEdificio);
                            myMapEdificio.set(value.descriEdificio, value.idEdificio);
                        });
                    });
                    DependenciaService.listar().success(function (d) {
                        angular.forEach(d, function (value, key) {
                            $scope.nestedItemsLevelDependencia.push(value.descriDependencia);
                            myMapDependencia.set(value.descriDependencia, value.idDependencia);
                        });
                    });
                    ComponenteService.listar().success(function (d) {
                        angular.forEach(d, function (value, key) {
                            $scope.nestedItemsLevelComponente.push(value.descriComponente);
                            myMapComponente.set(value.descriComponente, value.idComponente);
                        });
                    });
                };
//                $scope.onChanged = function () {
//                    $scope.nestedItemsLevelDependencia = [];
//                    $scope.idDependencia = "";
//                    TipoEdificioDepeService.listarPorIdTipoEdificio(myMapEdificio.get($scope.idEdificio)).success(function (data) {
////                        PlacaService.tipoPlacaPin(data.placa.idPlaca).success(function (d) {
//                        angular.forEach(data, function (value, key) {
//                            $scope.nestedItemsLevelDependencia.push(value.dependencia.descriDependencia);
//                            myMapDependencia.set(value.dependencia.descriDependencia, value.dependencia.idDependencia);
//                        });
////                        });
//                    });
//                };
                $scope.modificar = function (nodos) {
                    ComponenteService.listar().success(function (d) {
                        $scope.componentesMod = d;
                        $scope.nodoModificar.componente = nodos.componente;
                    });
                    DependenciaService.listar().success(function (d) {
                        $scope.dependenciasMod = d;
                        $scope.nodoModificar.dependencia = nodos.dependencia;
                    });
                    PlacaService.listar().success(function (d) {
                        $scope.placasMod = d;
                        $scope.nodoModificar.placa = nodos.placa;
                    });
                    EdificioService.listar().success(function (d) {
                        $scope.edificiosMod = d;
                        $scope.nodoModificar.edificio = nodos.edificio;
                    });
                    EntidadService.listar().success(function (d) {
                        $scope.entidadEdiMod = d;
                        $scope.nodoModificar.entidad = nodos.entidad;
                    });
                    $scope.nodoModificar.idNodo = nodos.idNodo;
                    $scope.nodoModificar.descriNodo = nodos.descriNodo;
                    $scope.nodoModificar.estadoComponente = false;
                    $scope.nodoModificar.estadoNodo = $scope.nodoAgregar.estadoNodo;

                    if (nodos.estadoNodo === true) {
//                    if (nodos.estadoComponente === true) { --> ORIGINAL
                        $("#estadoNod").prop("checked", true);
                        $scope.nodoModificar.estadoNodo = true;
//                        $scope.nodoModificar.estadoComponente = true; --> ORIGINAL
                    } else {
                        $("#estadoNod").prop("checked", false);
//                        $scope.nodoModificar.estadoComponente = false; --> ORIGINAL
                        $scope.nodoModificar.estadoNodo = false;
                    }

                    $("#modalModificar").modal("show");
                };

//                $scope.eliminar = function (edi) {
//                    var r = confirm("Seguro que quiere eliminar el nodo " + edi.descriNodo + "?");
//                    if (r === true) {
//                        NodoService.bajas(edi).success(function (data) {
//                            if (data === true) {
//                                swal('Eliminado!',
//                                        'Haz eliminado exitosamente.',
//                                        'success'
//                                        );
//                                $scope.listar();
//                            }
//                        });
//                    } else {
//                        swal('Cancelado',
//                                'Haz cancelado la eliminación',
//                                'error'
//                                );
//                    }
//                };

                $scope.eliminar = function (edi) {
                    swal({
                        title: "Mensaje del Sistema",
                        text: "Seguro que quiere eliminar el nodo " + edi.descriNodo + "?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Si",
                        cancelButtonText: "No",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                            function (isConfirm) {
                                if (isConfirm) {
                                    NodoService.eliminar(edi.idNodo).success(function (da) {
                                        if (da === false) {
                                            NodoService.bajas(edi).success(function (data) {
                                                if (data === true) {
                                                    swal('Eliminado!',
                                                            'Haz dado de baja exitosamente.',
                                                            'success'
                                                            );
                                                    $scope.listar();
                                                }
                                            }).error(function (e) {
                                                swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                                            });
                                        } else {
                                            swal("Mensaje del Sistema!", "Datos eliminados exitosamente", "success");
                                            $scope.listar();

                                        }
                                    });
                                } else {
                                    swal("Mensaje del Sistema", "Haz cancelado la operación!", "error");
                                }
                            });
                };

                $scope.$watch('filterText', function () {
                    $("#paginaActual").val("1");
                })
                $scope.$watch('componente', function () {
                    $("#paginaActual").val("1");
                })
                $scope.$watch('dependencia', function () {
                    $("#paginaActual").val("1");
                })
                $scope.$watch('placa', function () {
                    $("#paginaActual").val("1");
                })
                $scope.buscar();

            }]);