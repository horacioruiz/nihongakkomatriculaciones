'use strict'
app
        .controller('listPlanCtrl', ['$state', '$scope', 'config', 'PlanService', '$cookies', '$base64', 'NodoService', 'ParametroService', '$http', '$rootScope', function ($state, $scope, config, PlanService, $cookies, $base64, NodoService, ParametroService, $http, $rootScope) {
//                console.log("HOLAAA");

                $scope.plan = {};
                $scope.planAgregar = {};
                $scope.planModificar = {};
                $scope.planAgregar.nodo = {};
                $scope.planAgregar.parametro = {};
                $scope.planAgregar.tipoPlaca = {};
                var myMapNodo = new Map();
                var myMapParametro = new Map();
                $("#paginaActual").val("1");
                $scope.totalItems = 0;
                $scope.currentPage = 0;
                $scope.pageChangedFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };
                $scope.pageChangedSinFiltro = function () {
                    $("#paginaActual").val($scope.currentPage);
                    $scope.buscarSegunPagina();
                };
                $scope.limpiar = function () {
                    $scope.filterText = "";
                    $scope.dependencia = "";
                    $scope.componente = "";
                    $scope.parametro = "";
                    $scope.edificio = "";
                };
//              PAGINACION
                $scope.paginar = function () {
                    PlanService.count().success(function (data) {
                        $scope.catidad = data;
                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());
                        $("#paginacionFiltro").css("display", "none");
                        $("#paginacionSinFiltro").css("display", "inline");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };
                $rootScope.$on("$locationChangeStart", function (event, data) {
                    var pag = parseInt(data.replace("http://" + config.ipFront + ":8017/menu.html#/:pagina=", ""));
                    if (!isNaN(pag)) {
                        $("#paginaActual").val(pag);
                        $scope.buscarSegunPagina();
                    }
                });
                $scope.paginarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.dependencia === "" || $scope.dependencia === undefined) {
                        $scope.dep = null;
                    } else {
                        $scope.dep = $scope.dependencia;
                    }
                    if ($scope.componente === "" || $scope.componente === undefined) {
                        $scope.comp = null;
                    } else {
                        $scope.comp = $scope.componente;
                    }
                    if ($scope.parametro === "" || $scope.parametro === undefined) {
                        $scope.para = null;
                    } else {
                        $scope.para = $scope.parametro;
                    }
                    if ($scope.edificio === "" || $scope.edificio === undefined) {
                        $scope.edi = null;
                    } else {
                        $scope.edi = $scope.edificio;
                    }
                    PlanService.countFiltro($scope.filt, $scope.dep, $scope.comp, $scope.para, $scope.edi).success(function (data) {
                        $scope.catidad = data;
                        $scope.totalItems = $scope.catidad;
                        $scope.currentPage = parseInt($("#paginaActual").val());
                        $("#paginacionFiltro").css("display", "inline");
                        $("#paginacionSinFiltro").css("display", "none");
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };

                $scope.generarReporte = function () {
                    $scope.jsonData = $cookies.getObject("usuarioLogueado");
                    $scope.userLog = $scope.jsonData.usuarioUsuario;

                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.dependencia === "" || $scope.dependencia === undefined) {
                        $scope.dep = null;
                    } else {
                        $scope.dep = $scope.dependencia;
                    }
                    if ($scope.componente === "" || $scope.componente === undefined) {
                        $scope.comp = null;
                    } else {
                        $scope.comp = $scope.componente;
                    }
                    if ($scope.parametro === "" || $scope.parametro === undefined) {
                        $scope.para = null;
                    } else {
                        $scope.para = $scope.parametro;
                    }
                    if ($scope.edificio === "" || $scope.edificio === undefined) {
                        $scope.edi = null;
                    } else {
                        $scope.edi = $scope.edificio;
                    }
                    $http.get(config.backend + '/plan/generarReporte/' + $scope.filt + '/' + $scope.edi + '/' + $scope.dep + '/' + $scope.comp + '/' + $scope.para + '/' + $scope.userLog, {responseType: 'arraybuffer'})
                            .success(function (data) {
                                var file = new Blob([data], {type: 'application/pdf'});
                                var fileURL = URL.createObjectURL(file);
                                window.open(fileURL);
                            });
                };

                $scope.listarConFiltro = function () {
                    if ($scope.filterText === undefined || $scope.filterText === "") {
                        $scope.filt = null;
                    } else {
                        $scope.filt = $scope.filterText;
                    }
                    if ($scope.dependencia === "" || $scope.dependencia === undefined) {
                        $scope.dep = null;
                    } else {
                        $scope.dep = $scope.dependencia;
                    }
                    if ($scope.componente === "" || $scope.componente === undefined) {
                        $scope.comp = null;
                    } else {
                        $scope.comp = $scope.componente;
                    }
                    if ($scope.parametro === "" || $scope.parametro === undefined) {
                        $scope.para = null;
                    } else {
                        $scope.para = $scope.parametro;
                    }
                    if ($scope.edificio === "" || $scope.edificio === undefined) {
                        $scope.edi = null;
                    } else {
                        $scope.edi = $scope.edificio;
                    }
                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    PlanService.fetchFiltro(config.cantPaginacion, offSet, $scope.filt, $scope.dep, $scope.comp, $scope.para, $scope.edi).success(function (data) {
                        $scope.plan = data;
                        if ($scope.plan.length === 0) {
                            $("#detallePlan").html("<tr><td colspan='9'><center> NINGUN PLAN DETECTADO... </center></td></tr>")
                        } else {
                            $("#detallePlan").html("");
                            $("#detallePlanes").fadeIn("fast");
                        }
                        $scope.paginarConFiltro();
                    }
                    ).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }

                $scope.buscar = function () {
                    $scope.plan = [];
//                    location.href = "http://" + config.ipFront + ":8017/menu.html#/:pagina=1";
                    if ($scope.filterText !== "" || $scope.componente !== "" || $scope.dependencia !== "" || $scope.parametro !== "" || $scope.edificio !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
//                    }
                };
                $scope.buscarSegunPagina = function () {
                    $scope.plan = [];
                    if ($scope.filterText !== "" || $scope.componente !== "" || $scope.dependencia !== "" || $scope.parametro !== "" || $scope.edificio !== "") {
                        $scope.listarConFiltro();
                    } else {
                        $scope.listar();
                    }
                };
//                FIN DE PAGINACION

                $scope.listar = function () {
                    var offSet = (parseInt($("#paginaActual").val()) - 1) * config.cantPaginacion;
                    PlanService.fetch(config.cantPaginacion, offSet).success(function (data) {
                        $scope.plan = data;
                        $scope.paginar();
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                };
                $scope.onChanged = function () {
                    $scope.nestedItemsLevelPlacaPin = [];
                    NodoService.listarPorId(myMapNodo.get($scope.idNodo)).success(function (data) {
                        $scope.planAgregar.descriEdificio = data.edificio.descriEdificio;
                        $scope.planAgregar.descriPlaca = data.placa.descriPlaca;
                        $scope.planAgregar.descriDependencia = data.dependencia.descriDependencia;
                        $scope.planAgregar.descriComponente = data.componente.descriComponente;
                        $scope.planAgregar.tipoCompo = data.componente.tipoComponente.descriTipoComp;
                        $scope.cargarCombo();
//                        alert("CARGANDO COMBO")
                    });
                };
                $scope.registrar = function () {
                    //HORA
                    var horaInicial = $('#horaInicial').val(),
                            horaFinal = $('#horaFinal').val(),
                            hours = horaFinal.split(':')[0] - horaInicial.split(':')[0],
                            minutes = horaFinal.split(':')[1] - horaInicial.split(':')[1];
                    minutes = minutes.toString().length < 2 ? '0' + minutes : minutes;
                    if (minutes < 0) {
                        hours--;
                        minutes = 60 + minutes;
                    }
                    hours = hours.toString().length < 2 ? '0' + hours : hours;
                    //FIN HORA
                    if ($scope.planAgregar.horaIniPlan === "" || $scope.planAgregar.horaIniPlan === undefined || $scope.planAgregar.horaIniPlan === null ||
                            $scope.planAgregar.horaFinPlan === "" || $scope.planAgregar.horaFinPlan === undefined || $scope.planAgregar.horaFinPlan === null) {
                        swal("Mensaje del Sistema!", "Los datos de Hora Inicio Y Hora Fin no deben quedar vacío", "error");
                    } else {
                        var fechaEntrada = $scope.planAgregar.fechaIniPlan;
                        var fechaLimite = $scope.planAgregar.fechaFinPlan;
                        if ((new Date(fechaEntrada).getTime() < new Date(fechaLimite).getTime())) {
                            $scope.registrarPlanes();
//                            if (hours >= 0) {
//                                if (!isNaN(minutes)) {
//                                    alert("La hora es: " + hours + ":" + minutes + " SOY POSITIVO");
//                                }
//                            } else {
//                                if (!isNaN(minutes)) {
//                                    alert("La hora es: " + hours + ":" + minutes + " SOY NEGATIVO");
//                                }
//                            }
//                            alert('la fecha es menor ');
                        } else if ((new Date(fechaEntrada).getTime() === new Date(fechaLimite).getTime())) {
                            if (hours >= 0) {
                                if (!isNaN(minutes)) {
                                    $scope.registrarPlanes();
                                }
                            } else {
                                if (!isNaN(minutes)) {
                                    swal("Mensaje del Sistema!", "La Hora Fin debe ser mayor a la Hora Inicio.", "error");
                                }
                            }
                        } else {
                            swal("Mensaje del Sistema!", "La Fecha Inicio y Fecha Fin no deben quedar vacíos. Y la Fecha Fin no debe ser Mayor a la Fecha Inicio", "error");
                        }
                    }





//                    if ($scope.planAgregar.horaIniPlan === "" || $scope.planAgregar.horaIniPlan === undefined || $scope.planAgregar.horaIniPlan === null ||
//                            $scope.planAgregar.horaFinPlan === "" || $scope.planAgregar.horaFinPlan === undefined || $scope.planAgregar.horaFinPlan === null) {
//                        swal("Mensaje del Sistema!", "Los datos no deben quedar vacío", "error");
//                    } else {

//                    }
                };

                $scope.registrarPlanes = function () {
                    $scope.planAgregar.nodo = {};
                    $scope.planAgregar.parametro = {};

                    $scope.planAgregar.nodo.idNodo = myMapNodo.get($scope.idNodo);
                    $scope.planAgregar.parametro.idParametro = myMapParametro.get($scope.idParametro);
                    $scope.planAgregar.estadoPlan = true;
                    $scope.planAgregar.horaIniPlan = $("#horaIni").html();
                    $scope.planAgregar.horaFinPlan = $("#horaFin").html();
                    $scope.jsonUsu = $cookies.getObject("usuarioLogueado");
                    $scope.planAgregar.usuAltaPlan = $scope.jsonUsu.usuarioUsuario;
                    $scope.planAgregar.usuModPlan = $scope.jsonUsu.usuarioUsuario;


                    PlanService.crear($scope.planAgregar).success(function (data) {
                        if (data === true) {
                            swal("Mensaje del Sistema!", "Datos Agregados exitosamente", "success");
                            $('#modalAgregar').modal('hide');
                            $scope.listar();
                        } else {
                            swal("Mensaje del Sistema!", "Datos no Agregados, verifique que los campos no estén vacíos", "error");
                        }
                    }).error(function (e) {
                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                    });
                }
                $scope.atras = function () {
                    location.reload();
                };
                $scope.cargarCombo = function () {
                    //PERSONA FILTRO
                    $scope.nestedItemsLevelNodo = [];
                    $scope.nestedItemsLevelParametro = [];
                    NodoService.getNodoByAdministracion().success(function (d) {
                        angular.forEach(d, function (value, key) {
                            $scope.nestedItemsLevelNodo.push(value.descriNodo);
                            myMapNodo.set(value.descriNodo, value.idNodo);
                        });
                    });
//                    NO OLVIDAR QUE NO SE DEBE MODIFICAR LO QUE HAY REGISTRADO EN PLANES
                    ParametroService.listar().success(function (d) {
//                        alert($scope.planAgregar.descriComponente+"");
                        var descriComp = $scope.planAgregar.tipoCompo + "";
//                        alert(descriComp);
                        angular.forEach(d, function (value, key) {
                            if (descriComp.toUpperCase() === "PUERTA") {
                                console.log("IMRPESION -->> " + value.descriParametro.toUpperCase())
                                if (value.descriParametro.toUpperCase() === "APERTURA" || value.descriParametro.toUpperCase() === "CIERRE") {
                                    $scope.nestedItemsLevelParametro.push(value.descriParametro);
                                    myMapParametro.set(value.descriParametro, value.idParametro);
                                }
                            } else {
                                console.log("IMRPESION -->> " + value.descriParametro.toUpperCase())
                                if (value.descriParametro.toUpperCase() === "ENCENDIDO" || value.descriParametro.toUpperCase() === "APAGADO") {
                                    $scope.nestedItemsLevelParametro.push(value.descriParametro);
                                    myMapParametro.set(value.descriParametro, value.idParametro);
                                }
//                                $scope.nestedItemsLevelParametro.push(value.descriParametro);
//                                myMapParametro.set(value.descriParametro, value.idParametro);
                            }
                        });
                    });
                };
                $scope.agregar = function () {
                    $scope.planAgregar = {};
                    $scope.idParametro = "";
                    $scope.idNodo = "";
                    $scope.cargarCombo();
                };
                $scope.eliminar = function (plan) {
                    $scope.datoEliminar = {};
                    $scope.datoEliminar.idPlan = plan.idPlan;
                    $scope.jsonUsu = $cookies.getObject("usuarioLogueado");
                    $scope.datoEliminar.usuModPlan = $scope.jsonUsu.usuarioUsuario;
                    swal({
                        title: "Mensaje del Sistema",
                        text: "Seguro que quiere eliminar dicho Plan?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Si",
                        cancelButtonText: "No",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                            function (isConfirm) {
                                if (isConfirm) {
                                    PlanService.modEliminar($scope.datoEliminar.idPlan, $scope.datoEliminar.usuModPlan).success(function (data) {
                                        if (data === true) {
                                            swal("Mensaje del Sistema!", "Datos Modificados exitosamente", "success");
//                                $('#modalModificar').modal('hide');
                                            $scope.planModificar = {};
                                            $scope.listar();
                                        } else {
                                            swal("Mensaje del Sistema!", "Datos no Modificados", "error");
                                        }
                                    }).error(function (e) {
                                        swal("Mensaje del Sistema!", "No se pudo establecer conexión con el Servidor", "error");
                                    });
                                } else {
                                    swal("Mensaje del Sistema", "Haz cancelado la operación!", "error");
                                }
                            });
                };
                $scope.$watch('filterText', function () {
                    $("#paginaActual").val("1");
                });
                $scope.$watch('componente', function () {
                    $("#paginaActual").val("1");
                });
                $scope.$watch('dependencia', function () {
                    $("#paginaActual").val("1");
                });
                $scope.$watch('parametro', function () {
                    $("#paginaActual").val("1");
                });
                $scope.buscar();
            }]);